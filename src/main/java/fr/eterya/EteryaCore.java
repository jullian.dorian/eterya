package fr.eterya;

import fr.eterya.api.cache.character.CharacterCache;
import fr.eterya.api.cache.player.PlayerCache;
import fr.eterya.core.EteryaManager;
import fr.eterya.core.commands.CommandsManager;
import fr.eterya.core.listeners.ListenersManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class EteryaCore extends JavaPlugin {

    private static EteryaCore instance;

    private EteryaManager eteryaManager;

    @Override
    public void onEnable() {
        super.onEnable();
        instance = this;

        eteryaManager = new EteryaManager(this);
        eteryaManager.setup();

        //On charge les commandes et les events
        new CommandsManager(instance);
        new ListenersManager(instance);

        //On charge les joueurs en cas de reload
        loadPlayersCache();

        fine("[EteryaCore] has been loaded successfully.");
    }

    @Override
    public void onDisable() {
        super.onDisable();

        savePlayersAndChars();

        eteryaManager.save();

        //jedisConnector.destroy();
        fine("[EteryaCore] has been unloaded successfully.");
    }

    public static EteryaCore getApi(){
        return instance;
    }

    /*public JedisConnector getJedisConnector(){
        return jedisConnector;
    }*/

    private void loadPlayersCache(){
        for(Player player : Bukkit.getServer().getOnlinePlayers()){
            PlayerCache nextCache = new PlayerCache(player);
            nextCache.loadPlayerCacheFromSQL();
        }
    }

    private void savePlayersAndChars(){
        for(Player player : Bukkit.getServer().getOnlinePlayers()){
            PlayerCache nextCache = PlayerCache.valueOf(player);

            //Si un joueur est connecté sur un personnage on l'enregistre
            if(CharacterCache.valueOf(nextCache) != null){
                CharacterCache characterCache = CharacterCache.valueOf(player);
                characterCache.saveCharacterCache(player.getInventory());
            }
            //On sauvegarde le joueur
            nextCache.savePlayerCacheOnSQL();
        }
    }

    public EteryaManager getEteryaManager() {
        return eteryaManager;
    }

    /**
     * Send a fine message to console
     * @param message Set the message to send it
     */
    public void fine(String message){
        this.getServer().getConsoleSender().sendMessage(ChatColor.GREEN+message);
    }

    /**
     * Send a severe message to console
     * @param message Set the message to send it
     */
    public void severe(String message){
        this.getServer().getConsoleSender().sendMessage(ChatColor.RED+message);
    }

    /**
     * Send a info message to console
     * @param message Set the message to send it
     */
    public void info(String message){
        this.getServer().getConsoleSender().sendMessage(ChatColor.YELLOW+message);
    }

}