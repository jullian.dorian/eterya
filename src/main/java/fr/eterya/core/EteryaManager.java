package fr.eterya.core;

import fr.eterya.EteryaCore;
import fr.eterya.api.grade.Rank;
import fr.eterya.api.items.ItemDataBase;
import fr.eterya.api.npc.NPCManager;
import fr.eterya.api.params.Parametres;
import fr.eterya.api.selection.RegionSelection;
import fr.eterya.api.selection.region.Region;
import fr.eterya.api.sql.DataBase;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Arkas on 02/08/2017.
 * at 11:40
 */
public class EteryaManager {

    private EteryaCore eteryaCore;

    private Parametres parametres;
    private NPCManager npcManager;

    private Map<Rank, Scoreboard> boards = new HashMap<>();
    private Map<Player, RegionSelection> regionSelection = new HashMap<>();
    private List<Region> regions = new ArrayList<>();

    private DataBase dataBase;
    //private JedisConnector jedisConnector;

    private ItemDataBase itemDataBase;

    public EteryaManager(EteryaCore eteryaCore) {
        this.eteryaCore = eteryaCore;
    }

    public void setup(){
        //Instance de la base de donnée
        this.dataBase = new DataBase("jdbc:mysql://", "localhost", "eterya", "root", "");
        this.dataBase.connection();
        //jedisConnector = new JedisConnector("192.168.1.25", 0);
        //On charge les paramètres du fichier config
        this.parametres = new Parametres(this.eteryaCore);
        this.npcManager = new NPCManager(this.eteryaCore);
        //On charge les items de la base de donnée
        this.itemDataBase = new ItemDataBase();

        //On crée les TEAMS pour les grades
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        for(Rank rank : Rank.values()){
            if(boards.containsKey(rank)) return;
            Team team = scoreboard.registerNewTeam(rank.getName());
            team.setPrefix(rank.getTab());
            team.setDisplayName(rank.getTab());
            team.setCanSeeFriendlyInvisibles(false);
            boards.put(rank, scoreboard);
        }
    }

    public void save(){
        this.dataBase.deconnection();
    }

    public Parametres getParametres() {
        return parametres;
    }

    public NPCManager getNpcManager() {
        return npcManager;
    }

    public Map<Rank, Scoreboard> getBoards() {
        return boards;
    }

    public Map<Player, RegionSelection> getRegionSelection() {
        return regionSelection;
    }

    public List<Region> getRegions() {
        return regions;
    }

    public DataBase getDataBase() {
        return dataBase;
    }

    public ItemDataBase getItemDataBase() {
        return itemDataBase;
    }

    public void setItemDataBase(ItemDataBase itemDataBase) {
        this.itemDataBase = itemDataBase;
    }
}
