package fr.eterya.core.listeners;

import fr.eterya.EteryaCore;
import fr.eterya.core.listeners.entity.ListenerEntityDamage;
import fr.eterya.core.listeners.entity.ListenerEntityDeath;
import fr.eterya.core.listeners.inventory.InventoryClickList;
import fr.eterya.core.listeners.player.*;
import fr.eterya.core.listeners.region.ListenerPlayerInteractRegion;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;

/**
 * Created by Arkas on 01/05/2017.
 * at 16:41
 */
public class ListenersManager {

    private final EteryaCore eteryaCore;
    private final PluginManager pluginManager;

    public ListenersManager(EteryaCore eteryaCore){
        this.eteryaCore = eteryaCore;
        this.pluginManager = Bukkit.getPluginManager();
        registerListeners();
    }

    private void registerListeners(){

        //Selection
        this.pluginManager.registerEvents(new ListenerPlayerInteractRegion(), this.eteryaCore);

        //player
        this.pluginManager.registerEvents(new ListenerJoin(), this.eteryaCore);
        this.pluginManager.registerEvents(new ListenerQuit(), this.eteryaCore);
        this.pluginManager.registerEvents(new ListenerPlayerInteract(), this.eteryaCore);
        this.pluginManager.registerEvents(new ListenerAsyncChat(), this.eteryaCore);
        this.pluginManager.registerEvents(new ListenerXpChange(), this.eteryaCore);
        this.pluginManager.registerEvents(new ListenerMove(), this.eteryaCore);
        this.pluginManager.registerEvents(new ListenerFood(), this.eteryaCore);
        this.pluginManager.registerEvents(new ListenerChange(), this.eteryaCore);
        this.pluginManager.registerEvents(new ListenerDrop(), this.eteryaCore);

        //Entity
        this.pluginManager.registerEvents(new ListenerEntityDamage(), this.eteryaCore);
        this.pluginManager.registerEvents(new ListenerEntityDeath(), this.eteryaCore);

        //Inventory
        this.pluginManager.registerEvents(new InventoryClickList(this.eteryaCore), this.eteryaCore);

    }

}
