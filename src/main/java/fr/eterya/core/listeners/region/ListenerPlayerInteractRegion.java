package fr.eterya.core.listeners.region;

import fr.eterya.EteryaCore;
import fr.eterya.api.selection.RegionSelection;
import fr.eterya.api.selection.Selection;
import fr.eterya.api.utils.EteryaItem;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Map;

/**
 * Created by Arkas on 20/07/2017.
 * at 22:06
 */
public class ListenerPlayerInteractRegion implements Listener{

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerInteractRegion(PlayerInteractEvent event){

        final Player player = event.getPlayer();
        final Map<Player, RegionSelection> regions =  EteryaCore.getApi().getEteryaManager().getRegionSelection();

        if(event.getItem() == null || event.getItem().getType() == Material.AIR) return;

        if(event.getItem().isSimilar(EteryaItem.ITEM_SELECTOR.getItem())){
            event.setCancelled(true);
            if(!regions.containsKey(player)) regions.put(player, new RegionSelection(new Selection(null), new Selection(null)));

            switch (event.getAction()){
                case LEFT_CLICK_BLOCK:
                    final Block blockLeft = event.getClickedBlock();
                    final Selection selectionLeft = new Selection(blockLeft);
                    if(regions.containsKey(player)) regions.get(player).setLeftSelection(selectionLeft);
                    player.sendMessage("§aVous avez définit le premier point à :" +
                            "\n" + "§5"+selectionLeft.getStringLocation());
                    break;
                case RIGHT_CLICK_BLOCK:
                    final Block blockRight = event.getClickedBlock();
                    final Selection selectionRight = new Selection(blockRight);
                    if(regions.containsKey(player)) regions.get(player).setRightSelection(selectionRight);
                    player.sendMessage("§aVous avez définit le deuxième point à :" +
                            "\n" + "§5"+selectionRight.getStringLocation());
                    break;
            }

        }

    }

}
