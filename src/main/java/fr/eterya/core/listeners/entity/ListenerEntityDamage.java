package fr.eterya.core.listeners.entity;

import fr.eterya.api.cache.character.CharacterCache;
import fr.eterya.api.items.ReferencedItems;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.Random;

/**
 * Created by Arkas on 28/06/2017.
 * at 15:05
 */
public class ListenerEntityDamage implements Listener {

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event){
        Entity damager = event.getDamager();
        if(!(damager instanceof Player)) return;
        Player player = (Player) damager;

        CharacterCache characterCache = CharacterCache.valueOf(player);

        if(characterCache == null || characterCache.hasCreate()){
            for(ReferencedItems referencedItems : ReferencedItems.getIdMap().values()) {
                if (player.getItemInHand().equals(referencedItems.getItem())) {
                    int min_attack = referencedItems.getMinAttack();
                    int max_attack = referencedItems.getMaxAttack();

                    Random random = new Random();
                    int damage = random.nextInt(max_attack - min_attack) + min_attack;

                    event.setDamage(damage);

                    break;
                }
            }
        } else {
            event.setCancelled(true);
        }
    }
}
