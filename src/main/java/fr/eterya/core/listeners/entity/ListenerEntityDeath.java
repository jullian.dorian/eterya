package fr.eterya.core.listeners.entity;

import fr.eterya.api.cache.character.CharacterCache;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

import java.util.Random;

/**
 * Created by Arkas on 15/07/2017.
 * at 03:06
 */
public class ListenerEntityDeath implements Listener {

    @EventHandler
    public void onEntityDeathEvent(EntityDeathEvent event){
        Entity killer = event.getEntity().getKiller();
        if(killer instanceof Player){
            CharacterCache characterCache = CharacterCache.valueOf((Player) killer);
            Random random = new Random();
            int money = 10 + random.nextInt(25);
            characterCache.addMoney(money);
        }
    }
}
