package fr.eterya.core.listeners.player;

import fr.eterya.api.cache.character.CharacterCache;
import fr.eterya.api.utils.EteryaItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;

/**
 * Created by Arkas on 01/06/2017.
 * at 22:56
 */
public class ListenerMove implements Listener {

    @EventHandler
    public void moveEvent(PlayerMoveEvent event){

        Player player = event.getPlayer();
        Inventory inventory = player.getOpenInventory().getTopInventory();

        CharacterCache characterCache = CharacterCache.valueOf(player);

        if(characterCache != null && characterCache.hasCreate()) {
            if (inventory.getItem(0) == null || inventory.getItem(0).getType() == Material.AIR) {
                inventory.setItem(0, EteryaItem.ITEM_MENU.getItem());
                player.updateInventory();
            }
        }
    }
}
