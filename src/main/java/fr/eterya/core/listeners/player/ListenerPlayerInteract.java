package fr.eterya.core.listeners.player;

import fr.eterya.api.cache.player.inventory.AbstractInventoryAllCharacters;
import fr.eterya.api.cache.player.inventory.AbstractInventoryLoadCharacter;
import fr.eterya.api.utils.EteryaItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Arka on 04/05/2017.
 */
public class ListenerPlayerInteract implements Listener {

    @EventHandler
    public void listenerInteract(PlayerInteractEvent event){
        Player player = event.getPlayer();
        ItemStack itemStack = event.getItem();

        //Si l'item est null ou c'est de l'air
        if(itemStack == null || itemStack.getType() == Material.AIR) return;

        //Si c'est autre qu'un clic droit [vide|block]
        if(event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) return;

        if(itemStack.hasItemMeta()){
            //Si l'item cliquer est égal à l'item jouer
            if(itemStack.equals(EteryaItem.LIST_CHARACTERS.getItem())){
                //Ouvre le menu des personnages
                player.openInventory(new AbstractInventoryAllCharacters(player).getInventory());

            } else if(itemStack.equals(EteryaItem.PLAY.getItem())){
                //Ouvre le menu du choix des personnages
                player.openInventory(new AbstractInventoryLoadCharacter(player).getInventory());
            }
            //On cancel les évènenements
            event.setCancelled(true);
        }
    }
}
