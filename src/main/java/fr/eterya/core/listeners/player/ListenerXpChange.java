package fr.eterya.core.listeners.player;

import fr.eterya.api.cache.character.CharacterCache;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerExpChangeEvent;

/**
 * Created by Arkas on 24/05/2017.
 * at 15:05
 */
public class ListenerXpChange implements Listener {

    @EventHandler
    public void xpChange(PlayerExpChangeEvent event){

        Player player = event.getPlayer();

        if(CharacterCache.valueOf(player) != null){
            CharacterCache characterCache = CharacterCache.valueOf(player);
            characterCache.getCharacter().giveExperience(Math.round(event.getAmount()*4));
        }
        //ON retire l'experience gagné à 0 (pour éviter de gagner l'exp de minecraft)
        event.setAmount(0);
    }

}
