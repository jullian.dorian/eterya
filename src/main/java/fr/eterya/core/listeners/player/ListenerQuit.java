package fr.eterya.core.listeners.player;

import fr.eterya.api.cache.character.CharacterCache;
import fr.eterya.api.cache.player.PlayerCache;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Arka on 02/05/2017.
 */
public class ListenerQuit implements Listener {

    @EventHandler
    public void listenerQuitEvent(PlayerQuitEvent event){

        Player player = event.getPlayer();

        //On récupère le personnage actuel
        CharacterCache characterCache = CharacterCache.valueOf(player);
        //Si le personnage est inexistant ou null
        if(characterCache == null || !characterCache.hasCreate()){
            //On sauvegarde seulement le compte du joueur
            PlayerCache playerCache = PlayerCache.valueOf(player);
            playerCache.savePlayerCacheOnSQL();
        } else {
            //On sauvegarde en priorité le personnage est après le joueur
            PlayerCache playerCache = characterCache.getPlayerCache();

            characterCache.saveCharacterCache(player.getInventory());
            playerCache.savePlayerCacheOnSQL();
        }
    }
}
