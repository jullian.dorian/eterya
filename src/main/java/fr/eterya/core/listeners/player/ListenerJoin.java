package fr.eterya.core.listeners.player;

import fr.eterya.api.cache.player.PlayerCache;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Arka on 02/05/2017.
 */
public class ListenerJoin implements Listener {

    @EventHandler
    public void listenerJoinEvent(PlayerJoinEvent event){

        Player player = event.getPlayer();

        //On 'créer' le compte du joueur
        PlayerCache playerCache = new PlayerCache(player);
        playerCache.loadPlayerCacheFromSQL();
        playerCache.active();
        playerCache.setMenu();
    }
}
