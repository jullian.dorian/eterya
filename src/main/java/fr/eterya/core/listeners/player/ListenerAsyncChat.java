package fr.eterya.core.listeners.player;

import fr.eterya.api.cache.character.CharacterCache;
import fr.eterya.api.cache.player.PlayerCache;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Created by Arkas on 24/05/2017.
 * at 13:39
 */
public class ListenerAsyncChat implements Listener {

    @EventHandler
    public void asyncPlayerChat(AsyncPlayerChatEvent event){

        Player player = event.getPlayer();

        final PlayerCache playerCache = PlayerCache.valueOf(player);
        final CharacterCache characterCache = CharacterCache.valueOf(playerCache);
        if(!characterCache.hasCreate())
            event.setFormat(playerCache.getRank().getPrefix() + "§7"+player.getName() + " §f» §7"+event.getMessage());
        else
            event.setFormat("§6[§eLv."+characterCache.getCharacter().getLevel()+"§6] §7"+player.getName() + " §f» §7"+event.getMessage());
    }
}
