package fr.eterya.core.listeners.player;

import fr.eterya.api.cache.character.CharacterCache;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

/**
 * Created by Arkas on 18/07/2017.
 * at 23:03
 */
public class ListenerFood implements Listener{

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event){

        Player player = (Player) event.getEntity();

        CharacterCache characterCache = CharacterCache.valueOf(player);

        if(characterCache == null || !characterCache.hasCreate()) event.setCancelled(true);

    }

}
