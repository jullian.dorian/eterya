package fr.eterya.core.listeners.inventory;

import fr.eterya.EteryaCore;
import fr.eterya.api.cache.character.CharacterCache;
import fr.eterya.api.cache.character.inventory.AbstractInventoryMenuCharacter;
import fr.eterya.api.cache.player.inventory.AbstractInventoryAllCharacters;
import fr.eterya.api.cache.player.inventory.AbstractInventoryLoadCharacter;
import fr.eterya.api.utils.EteryaItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Arka on 04/05/2017.
 */
public class InventoryClickList implements Listener {

    private EteryaCore eteryaCore;

    public InventoryClickList(EteryaCore eteryaCore) {
        this.eteryaCore = eteryaCore;
    }

    @EventHandler
    public void inventoryClickList(InventoryClickEvent event){

        Player player = (Player) event.getWhoClicked();
        Inventory inventory = event.getClickedInventory();

        CharacterCache characterCache = new CharacterCache(player);

        if(characterCache.hasCreate()){
            AbstractInventoryMenuCharacter inventoryMenuCharacter = new AbstractInventoryMenuCharacter(characterCache);
            inventoryMenuCharacter.inventoryAction(event);

            if (event.getInventory().getTitle().equals("container.crafting")) {
                ItemStack itemStack = event.getCurrentItem();
                if (itemStack == null || itemStack.getType() == Material.AIR) return;
                if(player.getOpenInventory().getTopInventory().equals(inventory)) {
                    if (player.getOpenInventory().getTopInventory().getItem(0).isSimilar(EteryaItem.ITEM_MENU.getItem())) {
                        event.setCancelled(true);
                        player.closeInventory();
                        player.openInventory(inventoryMenuCharacter.getInventory());
                    }
                }
                return;
            }
        } else if(!characterCache.hasCreate()){
            AbstractInventoryLoadCharacter inventoryLoadCharacter = new AbstractInventoryLoadCharacter(player);
            inventoryLoadCharacter.inventoryAction(event);

            AbstractInventoryAllCharacters inventoryAllCharacters = new AbstractInventoryAllCharacters(player);
            inventoryAllCharacters.inventoryAction(event);
        } else {
            if(inventory.getTitle().equals("§eListes des items")){
                ItemStack itemStack = event.getCurrentItem();
                if(itemStack == null || itemStack.getType() == Material.AIR) return;
                event.setCancelled(true);

                if(itemStack.isSimilar(EteryaItem.NEXT.getItem()))
                    player.closeInventory();
                if(itemStack.isSimilar(EteryaItem.CLOSE.getItem()))
                    player.closeInventory();

                return;
            }
        }

    }
}
