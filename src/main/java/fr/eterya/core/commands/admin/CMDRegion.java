package fr.eterya.core.commands.admin;

import fr.eterya.EteryaCore;
import fr.eterya.api.selection.RegionSelection;
import fr.eterya.api.selection.region.Region;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;

/**
 * Created by Arkas on 20/07/2017.
 * at 22:39
 */
public class CMDRegion implements CommandExecutor {

    private EteryaCore eteryaCore;

    public CMDRegion(EteryaCore eteryaCore) {
        this.eteryaCore = eteryaCore;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        final Player player = (Player) sender;
        final Map<Player, RegionSelection> regions =  eteryaCore.getEteryaManager().getRegionSelection();
        final List<Region> cuboids =  eteryaCore.getEteryaManager().getRegions();

        if(args.length != 0){

            switch (args[0]) {

                case "create":

                    if (! regions.containsKey(player)) {
                        player.sendMessage("§cVous devez définir des points pour créer la region.");
                        return false;
                    }

                    if (args.length == 1) {
                        player.sendMessage("§cVous devez entrer un nom de région.");
                        return false;
                    }

                    final String name = args[1];

                    if (cuboids.contains(name)) {
                        player.sendMessage("§eLe nom de la région existe déjà.");
                        return false;
                    }

                    new Region(name, regions.get(player));
                    player.sendMessage("§aVous avez créer la région : §e" + name);

                    break;
            }
        }
        return false;
    }

}
