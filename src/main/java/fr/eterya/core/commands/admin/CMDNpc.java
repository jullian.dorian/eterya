package fr.eterya.core.commands.admin;

import com.mojang.authlib.GameProfile;
import fr.eterya.EteryaCore;
import fr.eterya.api.npc.NPCManager;
import net.minecraft.server.v1_11_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_11_R1.CraftServer;
import org.bukkit.craftbukkit.v1_11_R1.CraftWorld;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by Arkas on 03/08/2017.
 * at 14:24
 */
public class CMDNpc implements CommandExecutor {

    private EteryaCore eteryaCore;

    public CMDNpc(EteryaCore eteryaCore) {
        this.eteryaCore = eteryaCore;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if(args.length != 0){

            Player player = (Player) sender;

            String name = args[0];

            NPCManager npcManager = this.eteryaCore.getEteryaManager().getNpcManager();

            MinecraftServer nmsMS = ((CraftServer) Bukkit.getServer()).getServer();
            WorldServer nmsWS = ((CraftWorld) player.getWorld()).getHandle();
            GameProfile gameProfile = new GameProfile(UUID.randomUUID(), name);

            Entity entity = new EntityPlayer(nmsMS, nmsWS, gameProfile, new PlayerInteractManager(nmsWS));

            npcManager.createNpc(player, player.getEyeLocation(), entity);

        }

        return true;
    }
}
