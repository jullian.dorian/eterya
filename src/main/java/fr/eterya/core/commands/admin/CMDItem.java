package fr.eterya.core.commands.admin;

import fr.eterya.EteryaCore;
import fr.eterya.api.items.ReferencedItems;
import fr.eterya.api.utils.EteryaItem;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arkas on 01/05/2017.
 * at 10:21
 */
public class CMDItem implements CommandExecutor {

    private EteryaCore eteryaCore;

    public CMDItem(EteryaCore eteryaCore){
        this.eteryaCore = eteryaCore;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(sender instanceof Player){

            //Envoie la commande d'aide
            if(args.length == 0 || args[0].isEmpty() || args[0].equals("?") || args[0].equals("help")){
                sender.sendMessage(help());
            //Give un item pour un joueur spécifiée
            } else if(args[0].equals("give")) {
                if (args.length < 3) {
                    sender.sendMessage("§cIl manque des arguments pour continuer la commande !");
                    return false;
                }

                Player target = Bukkit.getPlayerExact(args[1]);
                ItemStack itemStack = null;
                int quantity = 1;

                try {
                    if ((ReferencedItems.getIdMap().size() < Integer.parseInt(args[2])) || (Integer.parseInt(args[2]) <= 0)) {
                        sender.sendMessage("§cL'ID de l'item ou sa quantité est supérieur à la table ou alors il est inférieur ou égal à zéro. §nRéessayez avec un autre chiffre.");
                        return false;
                    }
                    itemStack = ReferencedItems.valueOf(Integer.parseInt(args[2])).getItem();
                    if (args.length > 3) if (! args[3].isEmpty()) quantity = Integer.parseInt(args[3]);
                    itemStack.setAmount(quantity);
                } catch (NumberFormatException e) {
                    eteryaCore.severe(e.getMessage());
                }

                if (target.isOnline()) {
                    target.getInventory().addItem(itemStack);
                } else {
                    sender.sendMessage("§eLe joueur n'est pas en ligne.");
                }
            } else if(args[0].equals("list")){

                final List<ItemStack> contents = new ArrayList<>();
                final Inventory listItem = Bukkit.createInventory(null, 9*6, "§eListes des items");

                for(ReferencedItems referencedItem : ReferencedItems.getIdMap().values())
                    contents.add(referencedItem.getItem());

                for(int s = 0; s < contents.size(); s++){
                    if(s < (9*5-1)){
                        listItem.setItem(s, contents.get(s));
                    } else {
                        break;
                    }
                }

                listItem.setItem(53-8, EteryaItem.CLOSE.getItem());
                listItem.setItem(53, EteryaItem.NEXT.getItem());

                ((Player) sender).openInventory(listItem);
            }

            return false;
        //Reload des items lors d'un ajout / modification d'un item
        } else if(args[0].equalsIgnoreCase("reload") && sender.hasPermission("eterya.items.reload")){
            eteryaCore.getEteryaManager().getItemDataBase().reload(eteryaCore);
            sender.sendMessage("§aThe SQL items has been reload successfully !");
        }
        sender.sendMessage("You must be a player to execute this command.");
        return false;
    }

    private String[] help(){
        return new String[]{
                "§8-----------------------------------------------------",
                "§7/eitems §e<give> <player> <id_item> [qtt]",
                "§7/eitems §e<list>",
                "§7/eitems §r<reload>",
                "§7/eitems §e<?|help>",
                "§8-----------------------------------------------------"
        };
    }
}
