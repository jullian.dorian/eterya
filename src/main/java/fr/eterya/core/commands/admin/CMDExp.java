package fr.eterya.core.commands.admin;

import fr.eterya.api.cache.character.CharacterCache;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Arkas on 20/07/2017.
 * at 15:03
 */
public class CMDExp implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        if(args.length != 0) {
                String value = args[0];
                long exp = 0;
                try {
                    exp = Long.parseLong(value);
                } catch (NumberFormatException e) {
                    e.getMessage();
                }

                if (args.length == 1) {

                    Player player = (Player) sender;

                    CharacterCache characterCache = CharacterCache.valueOf(player);
                    if (characterCache.hasCreate())
                        characterCache.getCharacter().giveExperience(exp);

                } else {

                    if(args.length > 2) return false;

                    Player target = Bukkit.getPlayerExact(args[1]);

                    CharacterCache characterCache = CharacterCache.valueOf(target);
                    if (characterCache.hasCreate())
                        characterCache.getCharacter().giveExperience(exp);
                }
        }
        return false;
    }
}
