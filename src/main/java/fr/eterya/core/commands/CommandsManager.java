package fr.eterya.core.commands;

import fr.eterya.EteryaCore;
import fr.eterya.core.commands.admin.CMDExp;
import fr.eterya.core.commands.admin.CMDItem;
import fr.eterya.core.commands.admin.CMDNpc;
import fr.eterya.core.commands.admin.CMDRegion;
import fr.eterya.core.commands.all.CMDPing;

/**
 * Created by Arkas on 01/05/2017.
 * at 10:19
 */
public class CommandsManager {

    private EteryaCore eteryaCore;

    public CommandsManager(EteryaCore eteryaCore){
        this.eteryaCore = eteryaCore;
        registerCommands();
    }

    private void registerCommands(){
        this.eteryaCore.getCommand("eitems").setExecutor(new CMDItem(this.eteryaCore));
        this.eteryaCore.getCommand("ping").setExecutor(new CMDPing());
        this.eteryaCore.getCommand("npc").setExecutor(new CMDNpc(this.eteryaCore));

        this.eteryaCore.getCommand("exp").setExecutor(new CMDExp());

        this.eteryaCore.getCommand("region").setExecutor(new CMDRegion(this.eteryaCore));
    }

}
