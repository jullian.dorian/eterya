package fr.eterya.core.commands.all;

import fr.eterya.api.cache.player.PlayerCache;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Arkas on 18/06/2017.
 * at 14:51
 */
public class CMDPing implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        Player player = (Player) sender;

        if(player.hasPermission("eterya.ping.player") || player.isOp()) {
            if (args.length >= 1) {

                Player target = Bukkit.getPlayerExact(args[0]);

                if (target != null || target.isOnline()) {

                    PlayerCache targetCache = PlayerCache.valueOf(target);
                    if(targetCache == null) return true;

                    player.sendMessage("§ePong §f"+target.getName()+" §eà "+targetCache.getPing()+"ms! ");

                } else {
                    player.sendMessage("§cLe joueur n'est pas en ligne.");
                }
            } else {
                PlayerCache playerCache = PlayerCache.valueOf(player);

                player.sendMessage("§ePing.....");
                player.sendMessage("§ePong "+playerCache.getPing()+"ms!");
            }
        }
        return true;
    }
}
