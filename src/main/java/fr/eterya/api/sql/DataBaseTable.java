package fr.eterya.api.sql;

import fr.eterya.EteryaCore;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Arkas on 10/04/2017.
 * at 15:40
 */
public abstract class DataBaseTable {

    private String table = null;
    private Connection connection = EteryaCore.getApi().getEteryaManager().getDataBase().getConnection();

    public DataBaseTable(String table){
        this.table = table;
    }

    /**
     * Select ALL lignes of Table
     * Set string '*' if you want all lines
     * @param select
     * @return all lines
     */
    protected ResultSet selectAllQuery(String select){
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("SELECT "+select+" FROM "+getTable());
            return preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected ResultSet selectQuery(String select, String where){
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("SELECT "+select+" FROM "+getTable()+" WHERE "+where);
            return preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void updateQuery(String set, String where){
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("UPDATE "+getTable()+" SET "+set+" WHERE "+where);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void insertQuery(String set, String values){
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("INSERT INTO "+getTable()+" ("+set+") VALUES ("+values+")");
            preparedStatement.execute();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Supprimer la ligne à partir de la position indiqué
     * @param where
     */
    protected void deleteQuery(String where){
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("DELETE FROM "+getTable()+" WHERE (" + where + ")");
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    protected int selectInt(String select, String where){
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("SELECT "+select+" FROM "+getTable()+" WHERE "+where);
            ResultSet resultSet = preparedStatement.executeQuery();
            int value = 0;
            while(resultSet.next()) {
                value = resultSet.getInt(select);
            }
            preparedStatement.close();
            return value;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * Chech the connection
     * @return connection
     */
    protected Connection getConnection(){
        return connection;
    }

    /**
     * Get the table name
     * @return table
     */
    public String getTable(){
        return table;
    }

}
