package fr.eterya.api.sql;

import fr.eterya.EteryaCore;
import org.bukkit.Bukkit;

import java.sql.*;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Arkas on 04/04/2017.
 * at 11:41
 */
public class DataBase {

    private EteryaCore eteryaCore = EteryaCore.getApi();

    private String urlbase;
    private String host;
    private String database;
    private String username;
    private String password;
    private Connection connection;

    public DataBase(String urlbase, String host, String database,String username,String password){
        this.urlbase = urlbase;
        this.host = host;
        this.database = database;
        this.username = username;
        this.password = password;
    }

    /**
     * Connect to the BDD
     */
    public void connection(){
        try {
            //Connection au compte
            this.connection = DriverManager.getConnection(this.urlbase + this.host, this.username, this.password);
            //Preparation de la requête
            PreparedStatement preparedStatement = getConnection().prepareStatement("CREATE DATABASE IF NOT EXISTS eterya");
            //Execution
            preparedStatement.executeUpdate();
            //On ferme la requete et la connection
            preparedStatement.close();
            this.connection.close();
            //On redéfinit la connection à l'état nullable
            this.connection = null;

            //Connection au compte et à la base de donnée
            this.connection = DriverManager.getConnection(this.urlbase + this.host + "/" + this.database, this.username, this.password);
            eteryaCore.info("[EteryaCore] The BDD is now Online.");
            createTable();
        } catch (SQLException e){
            eteryaCore.severe("[EteryaCore] An Error on BDD of connection ! : "+e.getMessage());

            Bukkit.getServer().getPluginManager().disablePlugin(EteryaCore.getApi());
        }
    }

    /**
     * Deconnection of BDD
     */
    public void deconnection(){
        if(isConnected()){
            try {
                this.connection.close();
                eteryaCore.info("[EteryaCore] The BDD is now Offline.");
            } catch (SQLException e){
                eteryaCore.severe("[EteryaCore] An Error on BDD of deconnexion ! : "+e.getMessage());
            }
        }
    }

    /**
     * Check if the BDD is connected
     * @return connected
     */
    public boolean isConnected(){
        try {
            return ((this.connection == null) || (this.connection.isClosed()) || (this.connection.isValid(2))) ;
        } catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Get the connection
     * @return connection
     */
    public Connection getConnection(){
        return connection;
    }

    private final void createTable()  {
        //"ec_players", Stock les joueurs (Id, UUID, Grades, Banque, Argent, Permissions)
        String ec_players = "CREATE TABLE IF NOT EXISTS ec_players "
                + " (ID_PLAYER integer NOT NULL AUTO_INCREMENT,"
                + " UUID varchar(255) NOT NULL,"
                + " LV_RANK integer(11) NOT NULL,"
                + " MONEY integer(11) NOT NULL,"
                + " BANK text NOT NULL,"
                + " PERMISSIONS text NOT NULL,"
                + " PRIMARY KEY (ID_PLAYER),"
                + " UNIQUE KEY (UUID))";

        //"ec_characters", Stock les personnages(Nb de perso, Player, Classe, Inventaire, Location, Level, Exp, TotalExp, Stats)
        String ec_characters = "CREATE TABLE IF NOT EXISTS ec_characters"
                + " (ID_CHAR integer(11) NOT NULL AUTO_INCREMENT,"
                + " ID_PLAYER integer(11) NOT NULL,"
                + " MONEY integer(11) NOT NULL,"
                + " INVENTORY text NOT NULL,"
                + " LOCATION text NOT NULL,"
                + " LEVEL integer(11) NOT NULL,"
                + " EXPERIENCE integer(11) NOT NULL,"
                + " TOTAL_EXPERIENCE integer(11) NOT NULL,"
                + " ID_CARACS integer(11) NOT NULL,"
                + " ID_STATS integer(11) NOT NULL,"
                + " ID_SPELLS text NOT NULL,"
                + " PRIMARY KEY (ID_CHAR))";

        //"ec_items", Stock les items(Id, materialType, Damage, displayName, lore, flags, stats)
        String ec_items = "CREATE TABLE IF NOT EXISTS ec_items"
                + " (ID_ITEM integer(11) NOT NULL AUTO_INCREMENT,"
                + " TYPE integer(11) NOT NULL,"
                + " RARITY integer(11) NOT NULL,"
                + " MIN_ATTACK integer(4) NOT NULL, MAX_ATTACK integer(4) NOT NULL,"
                + " SPEED_ATTACK integer(11) NOT NULL, LEVEL_REQUIRED integer(4) NOT NULL,"
                + " MATERIAL_TYPE integer(11) NOT NULL,"
                + " DAMAGED integer(11) NOT NULL,"
                + " DISPLAY_NAME varchar(255) NOT NULL,"
                + " LORES text NOT NULL,"
                + " FLAGS text NOT NULL,"
                + " ENCHANTMENTS text NOT NULL,"

                + " VITALITE integer(4) NOT NULL,"
                + " REGENERATION integer(4) NOT NULL,"
                + " VITESSE integer(4) NOT NULL,"
                + " RESISTANCE_CAC integer(4) NOT NULL,"
                + " RESISTANCE_MAGIE integer(4) NOT NULL,"
                + " DOMMAGE integer(4) NOT NULL,"
                + " MAGIE integer(4) NOT NULL,"
                + " MANA integer(4) NOT NULL,"
                + " isUnbreakable boolean NOT NULL,"
                + " PRIMARY KEY (ID_ITEM))";

        //"ec_spells" : (Id, Name, Action, isItem, Item, Damage)
        String ec_spells = "CREATE TABLE IF NOT EXISTS ec_spells"
                + " (ID_SPELL integer(11) NOT NULL AUTO_INCREMENT,"
                + " NAME varchar(255) NOT NULL,"
                + " ACTION text NOT NULL,"
                + " ID_ITEM integer(11) NOT NULL,"
                + " DAMAGE integer(11) NOT NULL,"
                + " CONSUME_MANA integer(11) NOT NULL,"
                + " TIME_RESET integer(11) NOT NULL,"
                + " LEVEL_REQUIRED integer(11) NOT NULL,"
                + " LEVEL_OVERFLOW integer(11) NOT NULL,"
                + " isNeedItem boolean NOT NULL,"
                + " PRIMARY KEY (ID_SPELL))";

        String ec_friends = new String("CREATE TABLE IF NOT EXISTS ec_friends"
                + " (ID_PLAYER integer(11) NOT NULL,"
                + " PLAYERS integer(11) NOT NULL)");

        String ec_parametres = new String("CREATE TABLE IF NOT EXISTS ec_parameters"
                + " (SPAWN text NOT NULL,"
                + " TAB text NOT NULL)");

        //ec_pnj : (Id, Type, Location,

        List<String> requests = Arrays.asList(ec_players, ec_characters, ec_items, ec_spells, ec_friends, ec_parametres);

        try {
            Statement statement = this.connection.createStatement();
            for(String req : requests)
                statement.execute(req);

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
