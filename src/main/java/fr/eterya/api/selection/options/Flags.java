package fr.eterya.api.selection.options;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arkas on 20/07/2017.
 * at 23:13
 */
public class Flags {

    public enum Flag{
        PVP, PVE, ENTRY, EXIT;
    }

    public Flag getFlag(String name){
        return Flag.valueOf(name) != null ? Flag.valueOf(name) : null;
    }

    public List<Flag> getFlagList(List<String> names){
        final List<Flag> flags = new ArrayList<>();
        for(String name : names) {
            if (Flag.valueOf(name) == null) return null;
            flags.add(Flag.valueOf(name));
        }
        return flags;
    }

}
