package fr.eterya.api.selection.region;

import fr.eterya.EteryaCore;
import fr.eterya.api.selection.RegionSelection;
import fr.eterya.api.selection.options.Flags.Flag;

import java.util.List;

/**
 * Created by Arkas on 20/07/2017.
 * at 22:28
 */
public class Region {

    private String name;
    private RegionSelection selection;
    private List<Flag> flags;

    public Region(String name, RegionSelection selection) {
        this.name = name;
        this.selection = selection;
        EteryaCore.getApi().getEteryaManager().getRegions().add(this);
    }

    public String getName() {
        return name;
    }

    public RegionSelection getSelection() {
        return selection;
    }

    public List<Flag> getFlags() {
        return flags;
    }

    public void setFlags(List<Flag> flags) {
        this.flags = flags;
    }

    public void addFlags(Flag... value){
        for(Flag flags1 : value)
            if(!flags.contains(flags1))
                flags.add(flags1);
    }

    public boolean hasFlag(Flag flag){
        return flags.contains(flag);
    }
}
