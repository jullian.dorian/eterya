package fr.eterya.api.selection;

import org.bukkit.block.Block;

/**
 * Created by Arkas on 20/07/2017.
 * at 22:10
 */
public class Selection {

    private Block block;

    public Selection(Block block){
        this.block = block;
    }

    public Block getBlock() {
        return block;
    }

    public String getStringLocation(){
        return getBlock().getLocation().getX() + ", " + getBlock().getLocation().getY() + "," + getBlock().getLocation().getZ();
    }

}
