package fr.eterya.api.selection;

import java.util.List;

/**
 * Created by Arkas on 20/07/2017.
 * at 22:09
 */
public class RegionSelection {

    private Selection leftSelection;
    private Selection rightSelection;
    private List<Selection> polygoneSelection = null;

    public RegionSelection(Selection left, Selection right){
        this.leftSelection = left;
        this.rightSelection = right;
    }

    public RegionSelection(Selection left, List<Selection> rights){
        this.leftSelection = left;
        this.polygoneSelection = rights;
    }

    public Selection getLeftSelection() {
        return leftSelection;
    }

    public void setLeftSelection(Selection leftSelection) {
        this.leftSelection = leftSelection;
    }

    public Selection getRightSelection() {
        return rightSelection;
    }

    public void setRightSelection(Selection rightSelection) {
        this.rightSelection = rightSelection;
    }
}
