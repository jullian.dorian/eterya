package fr.eterya.api.params;

import fr.eterya.EteryaCore;
import fr.eterya.api.utils.FileBuilder;
import net.minecraft.server.v1_11_R1.PacketPlayOutPlayerListHeaderFooter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

/**
 * Created by Arkas on 19/07/2017.
 * at 16:06
 */
public class Parametres extends FileBuilder{

    public Location spawn_hub = null;
    public Location spawn_char = null;
    public PacketPlayOutPlayerListHeaderFooter packetTab = null;

    public Parametres(EteryaCore eteryaCore){
        super(eteryaCore, "configuration");
        setDefaultConfig();

        loadPositions();
    }

    private void setDefaultConfig(){

        //Spawn - Hub
        getConfig().set("spawn.hub.world", Bukkit.getWorlds().get(0).getName());
        getConfig().set("spawn.hub.x", 176f);
        getConfig().set("spawn.hub.y", 66f);
        getConfig().set("spawn.hub.z", 156f);
        getConfig().set("spawn.hub.yaw", 0);
        getConfig().set("spawn.hub.pitch", -2);

        //Spawn - Create character
        getConfig().set("spawn.char.world", Bukkit.getWorlds().get(0).getName());
        getConfig().set("spawn.char.x", 176f);
        getConfig().set("spawn.char.y", 68f);
        getConfig().set("spawn.char.z", 175f);
        getConfig().set("spawn.char.yaw", 0);
        getConfig().set("spawn.char.pitch", -2);

        //Tab List
        getConfig().set("tab.header", "&eEterya &f- &6Serveur MMORPG");
        getConfig().set("tab.footer", "&bhttp://eterya.fr");

        saveConfig();
    }

    private void loadPositions(){
        final World hub_world = Bukkit.getWorld(getConfig().getString("spawn.hub.world"));
        final World char_world = Bukkit.getWorld(getConfig().getString("spawn.char.world"));

        try{
            final float hub_x = Float.parseFloat(getConfig().getString("spawn.hub.x"));
            final float hub_y = Float.parseFloat(getConfig().getString("spawn.hub.y"));
            final float hub_z = Float.parseFloat(getConfig().getString("spawn.hub.z"));
            final int hub_yaw = Integer.parseInt(getConfig().getString("spawn.hub.yaw"));
            final int hub_pitch = Integer.parseInt(getConfig().getString("spawn.hub.pitch"));

            final float char_x = Float.parseFloat(getConfig().getString("spawn.char.x"));
            final float char_y = Float.parseFloat(getConfig().getString("spawn.char.y"));
            final float char_z = Float.parseFloat(getConfig().getString("spawn.char.z"));
            final int char_yaw = Integer.parseInt(getConfig().getString("spawn.char.yaw"));
            final int char_pitch = Integer.parseInt(getConfig().getString("spawn.char.pitch"));

            this.spawn_hub = new Location(hub_world, hub_x, hub_y, hub_z, hub_yaw, hub_pitch);
            this.spawn_char = new Location(char_world, char_x, char_y, char_z, char_yaw, char_pitch);
        } catch (NumberFormatException e){
            e.getMessage();
        }
    }
}
