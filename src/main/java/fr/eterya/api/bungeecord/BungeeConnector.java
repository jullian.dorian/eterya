package fr.eterya.api.bungeecord;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

/**
 * Created by Arkas on 10/04/2017.
 * at 18:30
 */
public class BungeeConnector {

    public BungeeConnector(){}

    public ByteArrayDataOutput sendPlayerServer(String targetServer){
        ByteArrayDataOutput dataOutput = ByteStreams.newDataOutput();
        dataOutput.writeUTF("Connect");
        dataOutput.writeUTF(targetServer);
        return dataOutput;
    }

}
