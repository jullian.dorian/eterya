package fr.eterya.api.utils;

import fr.eterya.api.items.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

/**
 * Created by Arkas on 24/05/2017.
 * at 11:47
 */
public enum EteryaItem {

    LIST_CHARACTERS(new ItemBuilder(Material.SKULL_ITEM, 1, (short) 1)
            .setDisplayName("§eListes des personnages §8(Right Click)").getItem()),
    FRIENDS(new ItemBuilder(Material.RED_ROSE)
            .setDisplayName("§cAmis §8(Bientôt)").getItem()),
    PLAY(new ItemBuilder(Material.IRON_SWORD)
            .setDisplayName("§6[>] §ePlay §6[<] §8(Right Click)")
            .addFlag(ItemFlag.HIDE_ATTRIBUTES).getItem()),
    SHOP(new ItemBuilder(Material.ENDER_CHEST)
            .setDisplayName("§aBoutique §8(Bientôt)")
            .addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true)
            .addFlag(ItemFlag.HIDE_ENCHANTS).getItem()),
    PARAMATERS(new ItemBuilder(Material.REDSTONE_COMPARATOR)
                .setDisplayName("§9Paramètres §8(Bientôt)").getItem()),

    CREATE_CHARACTER(new ItemBuilder(Material.IRON_SWORD)
            .setDisplayName("§aCreate a new character")
            .addFlag(ItemFlag.HIDE_ATTRIBUTES)
            .setLore(Arrays.asList("§7Click on this item for","§7create a new character !")).getItem()),

    DELETE_CHARACTER(new ItemBuilder(Material.INK_SACK, (short) 1)
            .setDisplayName("§cDelete this character")
            .setLores("","§cCette action est irreversible,","§ccela supprimera entièrement votre personnage","§cet tout son contenue.")
            .getItem()),

    ITEM_MENU(new ItemBuilder(Material.CHEST)
            .setDisplayName("§e[>] §nMenu§r §e[<]")
            .addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true)
            .setLore(Arrays.asList("","§7Click for open the menu."))
            .addFlag(ItemFlag.HIDE_ENCHANTS).getItem()),

    ACCEPTE(new ItemBuilder(Material.INK_SACK, (short) 10)
            .setDisplayName("§aAccepter").getItem()),
    DELETE(new ItemBuilder(Material.INK_SACK, (short) 1)
            .setDisplayName("§cDelete").getItem()),
    LEAVE(new ItemBuilder(Material.ARROW)
            .setDisplayName("§6Leave").getItem()),
    LEAVE2(new ItemBuilder(Material.WOOD_DOOR)
            .setDisplayName("§6Retourner au menu précèdent").getItem()),
    NEXT(new ItemBuilder(Material.ARROW)
            .setDisplayName("§7Suivant").getItem()),
    CLOSE(new ItemBuilder(Material.WOOD_DOOR)
            .setDisplayName("§cFermer").getItem()),

    DISCONNECT(new ItemBuilder(Material.INK_SACK,(short) 1)
            .setDisplayName("§cDisconnect").getItem()),

    ITEM_SELECTOR(new ItemBuilder(Material.GOLD_HOE).getItem())
    ;

    private ItemStack itemStack;

    EteryaItem(ItemStack itemStack){
        this.itemStack = itemStack;
    }

    public ItemStack getItem(){
        return itemStack;
    }

    public ItemMeta getItemMeta(){
        return itemStack.getItemMeta();
    }

    public Material getMaterial(){
        return itemStack.getType();
    }

    public String getDisplayName(){
        return itemStack.getItemMeta().getDisplayName();
    }

}
