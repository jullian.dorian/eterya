package fr.eterya.api.utils;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

/**
 * Created by Arkas on 18/03/2017.
 * at 17:17
 */
public abstract class FileBuilder {

    private File file;
    private FileConfiguration fileConfiguration;

    public FileBuilder(Plugin plugin, String file){
        if(!plugin.getDataFolder().exists()){
            plugin.getDataFolder().mkdirs();
        }

        this.file = new File(plugin.getDataFolder(), file+".yml");

        try {
            this.file.createNewFile();
        } catch (IOException e){}

        this.fileConfiguration = YamlConfiguration.loadConfiguration(this.file);
    }

    public FileConfiguration getConfig(){
        return fileConfiguration;
    }

    public void saveConfig(){
        try{
            fileConfiguration.save(this.file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
