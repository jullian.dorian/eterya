package fr.eterya.api.cache.player;

import fr.eterya.EteryaCore;
import fr.eterya.api.cache.player.account.Account;
import fr.eterya.api.grade.Rank;
import fr.eterya.api.inventory.util.Content;
import fr.eterya.api.packets.PacketTabList;
import fr.eterya.api.sql.DataBaseTable;
import fr.eterya.api.utils.EteryaItem;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Arkas on 11/03/2017.
 * at 19:08
 */
public class PlayerCache extends DataBaseTable {

    private static Map<Player, PlayerCache> playerCacheMap = new HashMap<>();

    private Player player;
    private int ID;
    private String name;
    private Account account;
    private Rank rank;

    public PlayerCache(String uuid){
        this(Bukkit.getPlayer(uuid));
    }

    public PlayerCache(Player player){
        super("ec_players");
        this.player = player;
    }

    /**
     * Création d'un cache joueur vierge
     */
    protected void createPlayerCache(){
        if(hasCache()) return;

        //On insert le joueur dans la BDD avec les paramètres par défaut
        insertQuery("UUID, LV_RANK, MONEY, BANK, PERMISSIONS", "'"+player.getUniqueId().toString()+"','1','0','null','null'");

        int id = selectInt("ID_PLAYER", "UUID = '"+player.getUniqueId().toString()+"'");
        //On Créer le joueur
        createPlayer(id, Rank.JOUEUR, new Account(0, Bukkit.createInventory(player, 9*6, "§a"+name+"§e's Bank")));

        EteryaCore.getApi().fine("Creation du compte par defaut de "+getName()+" avec succes.");
    }

    /**
     * Load the player from the BDD via Table ec_players
     * and create a PlayerCache
     */
    public void loadPlayerCacheFromSQL(){
        if(hasCache()) return;
        if(isExistOnSQL()) {
            int id_player = 1, lv_rank = 1, money = 0;
            try {
                ResultSet resultSet = selectQuery("ID_PLAYER, LV_RANK, MONEY", "UUID = '"+player.getUniqueId().toString()+"'");

                while(resultSet.next()){
                    id_player = resultSet.getInt("ID_PLAYER");
                    lv_rank = resultSet.getInt("LV_RANK");
                    money = resultSet.getInt("MONEY");
                }
                Rank rank = Rank.convertLevel(lv_rank);
                Account account = new Account(money, Bukkit.createInventory(null, 9, "okay"));

                resultSet.getStatement().close();

                createPlayer(id_player, rank, account);

                EteryaCore.getApi().fine("Le joueur a ete charger !");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return;
        }
        //Creation du player cache par défaut.
        createPlayerCache();
    }

    /**
     * Save the player on the BDD via Table ec_players
     * and remove player from PlayerCache
     */
    public void savePlayerCacheOnSQL(){
        Rank rank = getRank();
        int money = getAccount().getMoney();
        updateQuery("LV_RANK = "+rank.getLevel()+", MONEY = "+money, "UUID = '"+player.getUniqueId().toString()+"'");

        removePlayer();
    }

    /**
     * If the player is contains on BDD
     * @return hasAccount
     */
    private boolean isExistOnSQL(){
        try {
            ResultSet resultSet = selectQuery("UUID", "UUID = '"+player.getUniqueId().toString()+"'");
            boolean isExist = resultSet.next();

            resultSet.getStatement().close();
            return isExist;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Create a playercache and save this
     * @param id_player Id of the player in SQL
     * @param rank The rank of the player in SQM
     * @param account The account (Bank/money) in the SQL
     */
    private void createPlayer(int id_player, Rank rank, Account account){
        if(!hasCache()){
            this.ID = id_player;
            this.name = player.getName();
            this.rank = rank;
            this.account = account;
            playerCacheMap.put(player, this);
        }
    }

    private void removePlayer(){
        if(player == null) return;
        if(hasCache()){
            playerCacheMap.remove(player);
            EteryaCore.getApi().fine("Suppression du compte du joueur §f"+player.getName());
        }
    }

    public Player getPlayer(){
        return player;
    }

    public PlayerCache getCache(){
        return playerCacheMap.get(player);
    }

    public void active(){
        if(!player.isOp()) player.setGameMode(GameMode.ADVENTURE);
        player.setHealth(20d);
        player.setFoodLevel(20);
        player.setExp(0);
        player.setTotalExperience(0);
        player.setLevel(0);

        //Modification de la tablist pour les joueurs
        player.setScoreboard(EteryaCore.getApi().getEteryaManager().getBoards().get(rank));
        EteryaCore.getApi().getEteryaManager().getBoards().get(rank).getTeam(rank.getName()).addPlayer(player);

        //Modification de la tablist du header/footer
        PacketTabList packetTabList = new PacketTabList(EteryaCore.getApi().getEteryaManager().getParametres());
        packetTabList.sendPacket(player);


        //On téléporte le joueur au spawn/hub
        player.teleport(EteryaCore.getApi().getEteryaManager().getParametres().spawn_hub);
    }

    public void setMenu(){
        //On supprime l'inventaire
        player.getInventory().clear();
        //On set l'inventaire du joueur
        Content[] content = new Content[5];
        //Listes des personnages
        content[0] = new Content(0, EteryaItem.LIST_CHARACTERS.getItem());
        //Amis
        content[1] = new Content(2, EteryaItem.FRIENDS.getItem());
        //Jouer
        content[2] = new Content(4, EteryaItem.PLAY.getItem());
        //Boutique
        content[3] = new Content(6, EteryaItem.SHOP.getItem());
        //Paramètres
        content[4] = new Content(8, EteryaItem.PARAMATERS.getItem());

        for(Content content1 : content)
            player.getInventory().setItem(content1.getSlot(), content1.getItem());
    }

    /**
     * Get the ID on the sql database of Player
     * @return ID
     */
    public int getIdPlayer(){
        return ID;
    }

    public boolean hasCache() {
        return playerCacheMap.containsKey(player);
    }

    public int getPing() {
        return ((CraftPlayer) player).getHandle().ping;
    }

    public boolean hasBypass() {
        if (player.isOp() || player.hasPermission("eterya.bypass")) return true;
        return false;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setRank(Rank rank){ this.rank = rank; }

    public Rank getRank() {
        return rank;
    }

    public Account getAccount(){
        return account;
    }

    public static PlayerCache valueOf(String uuid){
        return playerCacheMap.get(Bukkit.getPlayer(uuid));
    }
    public static PlayerCache valueOf(Player player) {
        return playerCacheMap.get(player);
    }
    public static PlayerCache valueOf(int ID){
        if(playerCacheMap.containsValue(ID)){
            for(PlayerCache playerCache : playerCacheMap.values()){
                if(playerCache.getIdPlayer() == ID){
                    return playerCache;
                }
            }
        }
        return null;
    }
}
