package fr.eterya.api.cache.player.inventory;

import fr.eterya.api.cache.character.Character;
import fr.eterya.api.cache.character.CharacterCache;
import fr.eterya.api.inventory.util.AbstractInventory;
import fr.eterya.api.inventory.util.Content;
import fr.eterya.api.items.ItemBuilder;
import fr.eterya.api.utils.EteryaItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Arka on 03/05/2017.
 */
public class AbstractInventoryLoadCharacter extends AbstractInventory {

    private CharacterCache characterCache;

    public AbstractInventoryLoadCharacter(Player player){
        super(9, "§eChoose an character");
        this.characterCache = new CharacterCache(player);
        setContent(content());
    }

    @Override
    protected Content[] content(){
        final Content[] contents = new Content[getSize()];

        final List<Character> characters = characterCache.getAllCharacters();

        if(characters.isEmpty() || characters == null || characters.size() == 0) {
            contents[0] = new Content(4, EteryaItem.CREATE_CHARACTER.getItem());
            return contents;
        } else {
            int value = 0;

            //Si le nombre de personnage créer est inférieur à 5
            if(characters.size() < 5) {
                //On définie le nombre de création possible du personnage
                for (int s = 2; s < 7; s++) {
                    //J'ajoute à mon content l'itemStack "Créer un personnage"
                    contents[value] = new Content(s, EteryaItem.CREATE_CHARACTER.getItem());
                    value++;
                }
            }
            int add_slot = 0;
            for(Character character : characters){
                //DAPRES MINUSKUBE IL FAUT PARSE LE TITRE OU STOCKER + DE VALEUR POUR RECUPERER LE INT ID VOILA
                ItemStack icon = new ItemBuilder(Material.SKULL_ITEM, 1, (short) add_slot)
                        .setDisplayName("§6[>] §eCharacter N°"+(add_slot+1)+" §6[<]")
                        .setLore(Arrays.asList("§cClic droit pour supprimer le", "§cjoueur et son contenue.",
                                "",
                                "§e- §6Level: "+character.getLevel(),
                                "§e- §bExpérience: "+character.getExperience(),
                                "§e- §9Total Expérience: "+character.getTotalExperience()))
                        .getItem();
                contents[add_slot] = new Content((add_slot+2), icon);
                add_slot++;
            }
        }
        return contents;
    }

    @Override
    public void inventoryAction(InventoryClickEvent event) {

        //Selection du personnage ou création
        if(event.getInventory().getTitle().equalsIgnoreCase(getTitle())){
            ItemStack current = event.getCurrentItem();

            //Si l'item n'est pas égal à null
            if(current == null || current.getType() == Material.AIR) return;
            event.setCancelled(true);

            Player player = (Player) event.getWhoClicked();

            if (current.isSimilar(EteryaItem.CREATE_CHARACTER.getItem())) {
                player.closeInventory();
                characterCache.createCharacterCache();
                return;
            } else {
                if (current == null || current.getType() == Material.AIR) return;
                if(!event.getClickedInventory().getTitle().equals(getTitle())) return;

                Character character = characterCache.getAllCharacters().get(event.getSlot()-2);

                if(event.getClick() == ClickType.RIGHT){
                    characterCache.deleteCharacterCache(character);
                    player.closeInventory();
                    player.sendMessage("§aVous venez de supprimer votre personnage.");
                    return;
                }

                player.closeInventory();
                player.getInventory().clear();
                characterCache.loadCharacter(character);
            }
            return;
        }
    }


}
