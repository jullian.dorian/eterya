package fr.eterya.api.cache.player.account;

import org.bukkit.inventory.Inventory;

/**
 * Created by Arkas on 23/03/2017.
 * at 12:49
 */
public class Account implements IAccount {

    private int money;
    private Inventory bank;

    public Account(int money, Inventory bank){
        this.money = money;
        this.bank = bank;
    }

    /**
     * Get the money of player
     *
     * @return money
     */
    @Override
    public int getMoney() {
        return money;
    }

    /**
     * Set the money of player
     *
     * @param value
     */
    @Override
    public void setMoney(int value) {
        this.money = value;
    }

    /**
     * Add the money of player
     *
     * @param value
     */
    @Override
    public void addMoney(int value) {
        this.money += value;
    }

    /**
     * Remove the money of player
     *
     * @param value
     */
    @Override
    public void removeMoney(int value) {
        this.money -= value;
    }

    /**
     * Get the bank inventory of player
     *
     * @return bank
     */
    @Override
    public Inventory getBank() {
        return bank;
    }

    /**
     * Set the bank inventory of player
     *
     * @param bank
     */
    @Override
    public void setBank(Inventory bank) {
        this.bank = bank;
    }
}
