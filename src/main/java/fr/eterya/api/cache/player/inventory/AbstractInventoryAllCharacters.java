package fr.eterya.api.cache.player.inventory;

import fr.eterya.api.cache.character.Character;
import fr.eterya.api.cache.character.CharacterCache;
import fr.eterya.api.inventory.util.AbstractInventory;
import fr.eterya.api.inventory.util.Content;
import fr.eterya.api.items.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * Created by Arkas on 02/08/2017.
 * at 16:27
 */
public class AbstractInventoryAllCharacters extends AbstractInventory {

    private CharacterCache characterCache;

    public AbstractInventoryAllCharacters(Player player) {
        super(9*6, "§7Listes des personnages");
        this.characterCache = new CharacterCache(player);
        setContent(content());
    }

    @Override
    protected Content[] content() {
        final Content[] contents = new Content[getSize()];

        List<Character> characters = characterCache.getAllCharacters();

        if(characters == null || characters.isEmpty())
            return null;

        for(int s = 0; s < contents.length; s++){
            contents[s] = new Content(s, new ItemBuilder(Material.STAINED_GLASS_PANE, (short) 6)
                    .setDisplayName(" ").addFlag(ItemFlag.HIDE_ATTRIBUTES).getItem());
        }

        int number = 1;
        int slot = 0;
        for(Character character : characters){

            ItemStack head = new ItemBuilder(Material.SKULL_ITEM, 1, (short) (number-1))
                    .setDisplayName("§6Character N*§f"+number)
                    .getItem();

            ItemStack infos = new ItemBuilder(Material.PAPER)
                    .setDisplayName("§6Informations")
                    .setLores("","§e- §6Level: "+character.getLevel(),
                            "§e- §bExpérience: "+character.getExperience(),
                            "§e- §9Total Expérience: "+character.getTotalExperience())
                    .getItem();

            ItemStack guild = new ItemBuilder(Material.BANNER)
                    .setDisplayName("§5Guilde §cComing Soon")
                    .getItem();

            ItemStack succes = new ItemBuilder(Material.PAINTING)
                    .setDisplayName("§eSuccès §cComing Soon")
                    .getItem();

            contents[slot] = new Content(slot, head);
            contents[(slot+9)] = new Content((slot+9), infos);
            contents[(slot+9*2)] = new Content((slot+9*2), guild);
            contents[(slot+9*3)] = new Content((slot+9*3), succes);
            number++;
            slot += 2;
        }

        return contents;
    }

    @Override
    public void inventoryAction(InventoryClickEvent event) {

        if(event.getInventory().getTitle().equals(getTitle())){
            event.setCancelled(true);
            return;
        }
    }
}
