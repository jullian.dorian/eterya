package fr.eterya.api.cache.player.account;

import org.bukkit.inventory.Inventory;

/**
 * Created by Arkas on 23/03/2017.
 * at 12:49
 */
public interface IAccount {

    /*
    THE ACCOUNT OF PLAYER (ALL CHARACTERS)
    - MONEY
    - BANK
     */

    /**
     * Get the money of player
     * @return money
     */
    int getMoney();

    /**
     * Set the money of player
     * @param value
     */
    void setMoney(int value);

    /**
     * Add the money of player
     * @param value
     */
    void addMoney(int value);

    /**
     * Remove the money of player
     * @param value
     */
    void removeMoney(int value);

    /**
     * Get the bank inventory of player
     * @return bank
     */
    Inventory getBank();

    /**
     * Set the bank inventory of player
     * @param bank
     */
    void setBank(Inventory bank);
}
