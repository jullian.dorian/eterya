package fr.eterya.api.cache.character.inventory;

import fr.eterya.api.cache.character.CharacterCache;
import fr.eterya.api.inventory.util.AbstractInventory;
import fr.eterya.api.inventory.util.Content;
import fr.eterya.api.items.ItemBuilder;
import fr.eterya.api.utils.EteryaItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

import static org.bukkit.Material.*;
import static org.bukkit.Material.IRON_CHESTPLATE;

/**
 * Created by Arka on 03/05/2017.
 */
public class AbstractInventoryMenuCharacter extends AbstractInventory {

    private CharacterCache characterCache;

    public AbstractInventoryMenuCharacter(CharacterCache characterCache) {
        super(9*3, "§eMenu");
        this.characterCache = characterCache;
        setContent(content());
    }

    /*
    [STATS (0)] ; x ; x ; x ; x ; x ; x ; [spell(8)] ;
    [QUEST(9)] x ; x ; x ; x ; x ; x ; x ; [Infos Caracs(17)} ;
    [DECONNEXION(18)] x ; x ; x ; x ; x ; x ; x ; [Armure Caracs(26)] ;
     */

    @Override
    protected Content[] content() {
        final Content[] contents = new Content[getSize()];

        //Statistiques
        contents[0] = new Content(0, new ItemBuilder(PAPER)
                .setDisplayName("§eStatistiques")
                .setLore(Arrays.asList("","§7See your statistiques of","§7your character"))
                .getItem());

        //Spells
        contents[1] = new Content(8, new ItemBuilder(STICK)
                .setDisplayName("§eSpells")
                .setLore(Arrays.asList("",""))
                .getItem());

        //Quests
        contents[2] = new Content(9, new ItemBuilder(WRITTEN_BOOK)
                .setDisplayName("§6Quests log")
                .setLore(Arrays.asList("","§7See your quests log"))
                .getItem());

        contents[6] = new Content(13, new ItemBuilder(BEDROCK)
                .setDisplayName("§7Pierres")
                .setLores("","§fSee your all pierres.").getItem());

        //Infos character
        contents[3] = new Content(17, new ItemBuilder(SKULL_ITEM, (short) 3)
                .setDisplayName("§cCharacter Infos")
                .setLore(Arrays.asList("","§cHealth:"))
                .setOwner(characterCache.getName())
                .getItem());

        //Deconnexion
        contents[4] = new Content(18, EteryaItem.DISCONNECT.getItem());

        contents[5] = new Content(26, new ItemBuilder(IRON_CHESTPLATE)
                .setDisplayName("§7Armor stats")
                .setLore(Arrays.asList("","§fHelmet: ", "§fChestplate: ", "§fLeggings: ", "§fBoots: "))
                .getItem());

        return contents;
    }

    @Override
    public void inventoryAction(InventoryClickEvent event) {

        if(event.getInventory().getTitle().equalsIgnoreCase("§eMenu")){

            Player player = (Player) event.getWhoClicked();

            ItemStack itemStack = event.getCurrentItem();
            if(itemStack == null || itemStack.getType() == Material.AIR) return;

            event.setCancelled(true);
            if(itemStack.hasItemMeta()) {
                if(itemStack.equals(EteryaItem.DISCONNECT.getItem())){
                    //Deconnexion du joueur
                    CharacterCache characterCache = CharacterCache.valueOf(player);
                    characterCache.disconnect(player.getInventory());
                }
                player.closeInventory();
            }
            return;
        }

    }
}
