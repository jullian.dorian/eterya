package fr.eterya.api.cache.character;

import fr.eterya.api.cache.player.PlayerCache;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Arkas on 06/05/2017.
 * at 20:51
 */
public class Character {

    //Character
    private PlayerCache playerCache;
    private int ID;
    private int money;

    //Inventory
    private ItemStack[] inventory;

    //Levels
    private int level;
    private long experience;
    private long totalExperience;

    //Caracteristiques
    private int skillPoints;

    private Location lastLocation;

    public Character(final PlayerCache playerCache, int ID, int money, ItemStack[] inventory, int level, long experience, long totalExperience, int skillPoints, Location lastLocation) {
        this.playerCache = playerCache;
        this.ID = ID;
        this.money = money;
        this.inventory = inventory;
        this.level = level;
        this.experience = experience;
        this.totalExperience = totalExperience;
        this.skillPoints = skillPoints;
        this.lastLocation = lastLocation;
    }

    //Character

    public PlayerCache getPlayerCache() {
        return playerCache;
    }

    public void setPlayerCache(PlayerCache playerCache) {
        this.playerCache = playerCache;
    }

    public void setPlayerCache(Player player){
        PlayerCache playerCache = PlayerCache.valueOf(player);
        if(playerCache == null) this.playerCache = new PlayerCache(player);
        this.playerCache = playerCache;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void addMoney(int value){
        this.money += value;
    }

    //Inventory
    /**
     * Get the contents of character
     * @return inventory
     */
    public ItemStack[] getInventory() {
        return inventory;
    }

    public void setInventory(ItemStack[] inventory) {
        this.inventory = inventory;
    }

    //Level

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void addLevel(int value){
        this.level += value;
    }

    public void removeLevel(int value){
        this.level -= value;
    }

    public long getExperience() {
        return experience;
    }

    public void setExperience(long experience) {
        this.experience = experience;
    }

    public void addExperience(long value){
        this.experience += value;
    }

    public void removeExperience(long value){
        this.experience -= value;
    }

    public long getTotalExperience() {
        return totalExperience;
    }

    public void setTotalExperience(long totalExperience) {
        this.totalExperience = totalExperience;
    }

    public void addTotalExperience(long value){
        this.totalExperience += value;
    }

    public void removeTotalExperience(long value){
        this.totalExperience -= value;
    }

    public int getExperienceToLevel(){
        return (int) Math.round(100 * Math.pow(getLevel() + 1, 2));
    }
    public int getExperienceToLevel(int level){
        return (int) Math.round(100 * Math.pow(level + 1, 2));
    }

    //Caracs

    public int getSkillPoints() {
        return skillPoints;
    }

    public void setSkillPoints(int skillPoints) {
        this.skillPoints = skillPoints;
    }

    //Player

    public Location getLastLocation() {
        return lastLocation;
    }

    public void setLastLocation(Location lastLocation) {
        this.lastLocation = lastLocation;
    }

    public Player getPlayer(){
        return playerCache.getPlayer();
    }

    //Other
    public void giveExperience(long value){

        int func = getExperienceToLevel();

        //Si l'experience acquise est inférieur a l'experience max du niveau
        if((getExperience()+value) < func){
            addExperience(value);
            addTotalExperience(value);

            //On définit le nouveau chiffre entre 0 et 1 (0=min xp -> 1=max xp)
            double xp_to_mc = (getExperience() / (float) getExperienceToLevel(getLevel()));
            getPlayer().setExp((float)xp_to_mc);

            getPlayer().sendMessage("§e» §e+"+value+"§6XP");

        } //Si l'experience acquise est égal a l'experience max du niveau
        //Ce cas de figure est assez rare !
        else if((getExperience()+value) == func){
            setExperience(0);
            addLevel(1);
            addTotalExperience(value);

            setExperience(0);

            getPlayer().sendMessage("§e» §e+"+value+"§6XP");
            getPlayer().sendMessage("§e» Vous êtes maintenant niveau §6"+getLevel());
        } //Si l'experience acquise est supérieur a l'experience max du niveau
        else if((getExperience()+value) > func){
            addTotalExperience(value);
            long d = (getExperience()+value);

            //exemple : d > c (1308 > 400) (lv2)
            do {
                if(d < getExperienceToLevel(getLevel())) break;
                d -= getExperienceToLevel(getLevel());
                //On ajoute un niveau à chaque boucle
                addLevel(1);
            } while (d > func);
            //On set la nouvelle XP
            setExperience(d);
            double xp_to_mc = (d / (float) getExperienceToLevel(getLevel()));
            getPlayer().setExp((float) xp_to_mc);
            getPlayer().setLevel(getLevel());

            getPlayer().sendMessage("§e» §e+"+value+"§6XP");
            getPlayer().sendMessage("§e» Vous êtes maintenant niveau §6"+getLevel());
        }
    }

    @Deprecated
    public void giveLevel(int value){

        addLevel(value);
        double xp_to_mc = (getExperience() / (float) getExperienceToLevel(getLevel()));
        for(int carre = (getLevel()-value); carre < getLevel(); carre++){
            addTotalExperience(getExperienceToLevel(carre));
        }

        getPlayer().setLevel(getLevel());
        getPlayer().setExp((float) xp_to_mc);
    }

}
