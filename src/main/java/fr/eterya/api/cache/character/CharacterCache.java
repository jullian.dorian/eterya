package fr.eterya.api.cache.character;

import fr.eterya.EteryaCore;
import fr.eterya.api.cache.player.PlayerCache;
import fr.eterya.api.inventory.util.Content;
import fr.eterya.api.items.ReferencedItems;
import fr.eterya.api.sql.DataBaseTable;
import fr.eterya.api.utils.EteryaItem;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Arkas on 17/03/2017.
 * at 13:22
 */
public class CharacterCache extends DataBaseTable {

    private static Map<PlayerCache, CharacterCache> characterCacheMap = new HashMap<>();

    private PlayerCache playerCache;
    private Character character;

    public CharacterCache(Player player){
        this(PlayerCache.valueOf(player));
    }

    public CharacterCache(PlayerCache playerCache){
        super("ec_characters");
        this.playerCache = playerCache;
    }

    /* SQL */

    /**
     * Create a first character on SQL
     */
    public void createCharacterCache() {
        if(isExist()) return;

        int sizeCharacter = 0;

        //Si le joueur possède déjà un personnage on récupère le dernier chiffre
        if(hasOneCharacter()){
            ResultSet resultSet = selectQuery("ID_CHAR", "ID_PLAYER = "+playerCache.getIdPlayer());

            try {
                while (resultSet.next()){
                    sizeCharacter++;
                }
                resultSet.getStatement().close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

        if(sizeCharacter > 5){
            getPlayer().closeInventory();
            getPlayer().sendMessage("§4[BUG] "+"§cVous ne pouvez pas créer plus de personnages sur ce compte !");
            return;
        }

        //Location
        String loc = "world,173.5,68,168.5,-33,2";

        //Création d'un inventaire vierge
        String inventory = "1:0,1:0,1:0,1:0,1:0,1:0,1:0,1:0,1:0," +
                "1:0,1:0,1:0,1:0,1:0,1:0,1:0,1:0,1:0," +
                "1:0,1:0,1:0,1:0,1:0,1:0,1:0,1:0,1:0," +
                "1:0,1:0,1:0,1:0,1:0,1:0,1:0,1:0,1:0," +
                "1:0,1:0,1:0,1:0,1:0";
        ItemStack[] itemStacks = new ItemStack[47];

        for(int i = 0; i < itemStacks.length; i++)
            itemStacks[i] = new ItemStack(Material.AIR);

        //On ajoute le character
        insertQuery("ID_PLAYER, MONEY, INVENTORY, LOCATION, LEVEL, EXPERIENCE, TOTAL_EXPERIENCE, ID_CARACS, ID_STATS, ID_SPELLS"
                , " '"+getPlayerCache().getIdPlayer()+"','0','"+inventory+"','"+loc.toString()+"','0','0','0','0','0','0'");

        createCharacter(itemStacks);
    }

    public void loadCharacterCache(int ID) {
        if(isExist()) return;
        if(!hasOneCharacter()){
            createCharacterCache();
            return;
        }

        ResultSet resultSet = selectQuery("*", "ID_CHAR = "+ID);

        try{
            int id = 0;
            int money = 0;

            int level = 0;
            long experience = 0, total_experience = 0;
            int[] caracteristiques = new int[]{0, 100,0,0,0,0,0,0,0};
            Location last_location = new Location(Bukkit.getWorld("world"), 173.5, 68, 168.5, -33, 2);

            String column_inventory = "";

            while (resultSet.next()){
                id = resultSet.getInt("ID_CHAR");
                money = resultSet.getInt("MONEY");

                column_inventory = resultSet.getString("INVENTORY");

                level = resultSet.getInt("LEVEL");
                experience = resultSet.getLong("EXPERIENCE");
                total_experience = resultSet.getLong("TOTAL_EXPERIENCE");

                int caracs = resultSet.getInt("ID_CARACS");

                String[] stringLoc = resultSet.getString("LOCATION").split(",");
                World world = Bukkit.getServer().getWorld(stringLoc[0]);

                try{
                    last_location = new Location(world, Float.parseFloat(stringLoc[1]), Float.parseFloat(stringLoc[2]),
                            Float.parseFloat(stringLoc[3]), Float.parseFloat(stringLoc[4]), Float.parseFloat(stringLoc[5]));
                } catch (NumberFormatException e){
                    e.getMessage();
                }
                //cdb = new CaracteristiqueDataBase().getCaracs(id, caracs);
            }

            String[] columnInventory = column_inventory.split(",");
            ItemStack[] contents = new ItemStack[getPlayer().getInventory().getSize()];

            try {

                int s = 0;

                for(String itemContent : columnInventory){
                    String[] itemSplit = itemContent.split(":");
                    int ID2 = Integer.parseInt(itemSplit[0]);
                    int amount = Integer.parseInt(itemSplit[1]);

                    ReferencedItems referencedItems = ReferencedItems.valueOf(ID2);
                    ItemStack itemStack;
                    if(referencedItems.isItemExist()){
                        itemStack = referencedItems.getItem();
                        itemStack.setAmount(amount);
                        contents[s] = itemStack;
                    } else {
                        contents[s] = new ItemStack(Material.AIR);
                    }
                    s++;
                }

            } catch (NumberFormatException e){ e.getMessage(); }

            Character character = new Character(this.playerCache, id, money, contents, level, experience, total_experience, 0, last_location);

            loadCharacter(character);
            resultSet.getStatement().close();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Save the character on the BDD
     */
    public void saveCharacterCache(Inventory arg){
        //On récupère la location qu'on transforme en string
        Location location = getPlayer().getLocation();
        String loc = location.getWorld().getName()+","+String.valueOf(location.getX())+","+
        String.valueOf(location.getY())+","+String.valueOf(location.getZ())+","+String.valueOf(location.getYaw())+","+
        String.valueOf(location.getPitch());

        String inventory = ReferencedItems.convertInventoryToString(arg);

        updateQuery("MONEY = '" + getCharacter().getMoney() + "',INVENTORY = '" + inventory + "', LOCATION = '"+loc+"', LEVEL = '"+getCharacter().getLevel()+ "', "
                + "EXPERIENCE = '"+getCharacter().getExperience()+"', TOTAL_EXPERIENCE = '"+getCharacter().getTotalExperience()+"', "
                + "ID_CARACS = '1', ID_STATS = '0', ID_SPELLS = '0'", "ID_CHAR = '" + getCharacter().getID()
                + "' AND ID_PLAYER = '"+getPlayerCache().getIdPlayer()+"'");

        characterCacheMap.remove(playerCache);
    }

    public boolean deleteCharacterCache(Character character){
        if(isExist()) return false;

        deleteQuery("ID_CHAR = " + character.getID() + " AND " + "ID_PLAYER = " + getPlayerCache().getIdPlayer());

        return true;
    }

    /**
     * Récupère tous les personnages créers par le joueurs
     * @return list<character>
     */
    public final List<Character> getAllCharacters(){
        if(hasOneCharacter()){
            final List<Character> caches = new ArrayList<>();

            ResultSet resultSet = selectQuery("*", "ID_PLAYER = "+getPlayerCache().getIdPlayer());

            try{
                while (resultSet.next()){
                    int[] caracteristiques = new int[]{0, 100,0,0,0,0,0,0,0};
                    Location last_location = new Location(Bukkit.getWorld("world"), 173.5, 68, 168.5, -33, 2);

                    int id = resultSet.getInt("ID_CHAR");
                    int money = resultSet.getInt("MONEY");

                    String column_inventory = resultSet.getString("INVENTORY");

                    int level = resultSet.getInt("LEVEL");
                    long experience = resultSet.getLong("EXPERIENCE");
                    long total_experience = resultSet.getLong("TOTAL_EXPERIENCE");

                    int caracs = resultSet.getInt("ID_CARACS");

                    String[] stringLoc = resultSet.getString("LOCATION").split(",");
                    World world = Bukkit.getServer().getWorld(stringLoc[0]);

                    String[] columnInventory = column_inventory.split(",");
                    ItemStack[] contents = new ItemStack[getPlayer().getInventory().getSize()];

                    try{
                        last_location = new Location(world, Float.parseFloat(stringLoc[1]), Float.parseFloat(stringLoc[2]),
                                Float.parseFloat(stringLoc[3]), Float.parseFloat(stringLoc[4]), Float.parseFloat(stringLoc[5]));

                        int s = 0;

                        for(String itemContent : columnInventory){
                            String[] itemSplit = itemContent.split(":");
                            int ID = Integer.parseInt(itemSplit[0]);
                            int amount = Integer.parseInt(itemSplit[1]);

                            ReferencedItems referencedItems = ReferencedItems.valueOf(ID);
                            ItemStack itemStack;
                            if(referencedItems.isItemExist()){
                                itemStack = referencedItems.getItem();
                                itemStack.setAmount(amount);
                                contents[s] = itemStack;
                            } else {
                                contents[s] = new ItemStack(Material.AIR);
                            }
                            s++;
                        }

                    } catch (NumberFormatException e){
                        e.getMessage();
                    }
                    //cdb = new CaracteristiqueDataBase().getCaracs(id, caracs);

                    Character character = new Character(this.playerCache, id, money, contents, level, experience, total_experience, 0, last_location);
                    caches.add(character);
                }

            resultSet.getStatement().close();
            return caches;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return new ArrayList<>();
    }

    /* Character */

    /**
     * Create a default character
     */
    private void createCharacter(ItemStack[] inventory){
        int ID = selectInt("ID_CHAR", "ID_PLAYER = "+getPlayerCache().getIdPlayer());

        Location spawn = EteryaCore.getApi().getEteryaManager().getParametres().spawn_char;

        this.character = new Character(this.playerCache, ID, 0, inventory, 0,0,0,0, spawn);

        characterCacheMap.put(this.playerCache, this);
        EteryaCore.getApi().fine("Creation d'un personnage par defaut");

        //Application des paramètres au joueur
        Player player = getPlayer();

        player.getInventory().clear();

        player.setFoodLevel(20);

        player.setExp(0);
        player.setTotalExperience(0);
        player.setLevel(0);

        player.setGameMode(GameMode.ADVENTURE);

        player.teleport(spawn);
    }

    /**
     * Load the character with much parameters
     * @param character
     */
    public void loadCharacter(final Character character){
        this.character = character;

        characterCacheMap.put(this.playerCache, this);
        EteryaCore.getApi().fine("Chargement du personnage - ID : "+character.getID());

        //Application des paramètres au joueur
        Player player = getPlayer();

        Location last = character.getLastLocation();
        double exp = (character.getExperience() / (float) character.getExperienceToLevel());
        player.setLevel(character.getLevel());
        player.setExp((float) exp);
        player.setTotalExperience((int) character.getTotalExperience());

        player.setGameMode(GameMode.ADVENTURE);

        player.teleport(last);

        player.getInventory().setContents(character.getInventory());
        Inventory topInventory = player.getOpenInventory().getTopInventory();
        topInventory.setItem(0, new ItemStack(EteryaItem.ITEM_MENU.getItem()));

    }

    public boolean hasOneCharacter(){
        ResultSet resultSet = selectQuery("ID_CHAR", "ID_PLAYER = "+getPlayerCache().getIdPlayer());
        boolean value = false;
        try {
            value = resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                resultSet.getStatement().close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return value;
    }

    public void disconnect(Inventory inventory){
        saveCharacterCache(inventory);

        getPlayer().getInventory().clear();

        getPlayer().teleport(EteryaCore.getApi().getEteryaManager().getParametres().spawn_hub);

        //On set l'inventaire du joueur
        Content[] content = new Content[5];
        //Listes des personnages
        content[0] = new Content(0, EteryaItem.LIST_CHARACTERS.getItem());
        //Amis
        content[1] = new Content(2, EteryaItem.FRIENDS.getItem());
        //Jouer
        content[2] = new Content(4, EteryaItem.PLAY.getItem());
        //Boutique
        content[3] = new Content(6, EteryaItem.SHOP.getItem());
        //Paramètres
        content[4] = new Content(8, EteryaItem.PARAMATERS.getItem());

        for(Content content1 : content)
            getPlayer().getInventory().setItem(content1.getSlot(), content1.getItem());

    }

    private boolean isExist(){
        return characterCacheMap.containsKey(getPlayerCache());
    }
    public Character getCharacter(){
        return this.character;
    }

    public void addMoney(int value){
        getCharacter().setMoney(getCharacter().getMoney() + value);
        sendMessage("§e» §a+§6"+value+" §eEryaCoins");
    }

    public void update(){

    }

    /* PlayerCache */

    public PlayerCache getPlayerCache() {
        return this.playerCache;
    }

    /* Player */

    public Player getPlayer(){
        return getPlayerCache().getPlayer();
    }
    public String getName() {
        return getPlayer().getName();
    }
    public String getUUID(){
        return getPlayer().getUniqueId().toString();
    }

    /* Characher Cache */
    public boolean hasCreate(){
        return characterCacheMap.containsKey(playerCache);
    }

    public void sendMessage(String message){
        getPlayer().sendMessage(message);
    }
    public void sendMessage(String[] message){
        getPlayer().sendMessage(message);
    }

    /* Static */

    /**
     * Return the character cache
     * @param player
     * @return characterCache
     */
    public static CharacterCache valueOf(Player player){
        return valueOf(PlayerCache.valueOf(player));
    }

    /**
     * Return the CharacterCache
     * @param playerCache
     * @return characterCache
     */
    public static CharacterCache valueOf(PlayerCache playerCache){
        return characterCacheMap.containsKey(playerCache) ? characterCacheMap.get(playerCache) : null;
    }



}
