package fr.eterya.api.jedis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * Created by Arkas on 09/04/2017.
 * at 16:43
 */
public class JedisConnector {

    private JedisPool pool;

    private String ip = "127.0.0.1";
    private int port = 6379;

    public JedisConnector(String ip, int port){
        if(ip != null) this.ip = ip;
        if(port != 0) this.port = port;

        create();
    }
    public JedisPool getPool(){
        return pool;
    }

    private JedisPool create(){
        if(pool.isClosed() || pool == null) return pool = new JedisPool(ip, port);
        return pool;
    }

    public Jedis connect(){
        return pool.isClosed() ? null : pool.getResource();
    }

    public void disconnect(){
        if(!pool.isClosed()){
            pool.close();
        }
    }

    @Deprecated
    public void destroy(){
        disconnect();
        pool.destroy();
    }

}
