package fr.eterya.api.grade;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Arkas on 07/03/2017.
 * at 19:50
 */
public enum Rank {

    JOUEUR(1, "Joueur", "§8[Joueur] §r","§8", "eterya.rank.joueur"),
    VIP(2, "Vip", "§e[VIP] §r","§e[VIP] ", "eterya.rank.vip"),
    SUPER_VIP(3, "Super-Vip", "§6[VIP+] §r","§6[VIP+] ", "eterya.rank.svip"),
    HELPER(5, "Helper", "§a[Helper] §r","§a[Helper] ", "eterya.rank.helper"),
    STAFF(7, "Staff", "§f[§4Staff§f] §r","§f[§4Staff§f]§4 ", "eterya.rank.staff"),
    PARTENAIRE(8, "Partenaire", "§f[§aPartenaire§f] §r", "§a[Partenaire] ", "eterya.rank.partenaire"),
    MODERATEUR(10, "Moderateur", "§5[Modérateur] §r","§5[Modo] ", "eterya.rank.moderateur"),
    RESP_MODERATEUR(11, "Resp_Moderateur", "§7[Resp.Modérateur] §r","§7[Resp. Modo] ", "eterya.rank.resp-moderateur"),
    ADMIN(100, "Admin", "§c[Admin] §r","§c[Admin] ", "eterya.rank.admin");

    int level;
    String name, prefix, tab, permission;

    private static final Map<Integer, Rank> rankTypeMap = new HashMap<>();

    Rank(int level, String name, String prefix, String tab, String permission){
        this.level = level;
        this.name = name;
        this.prefix = prefix;
        this.tab = tab;
        this.permission = permission;
    }

    static {
        for(Rank values : Rank.values()){
            rankTypeMap.put(values.getLevel(), values);
        }
    }

    public int getLevel(){
        return level;
    }

    public String getName(){
        return name;
    }

    public String getPrefix(){
        return prefix;
    }

    public String getTab(){
        return tab;
    }

    public String getPermission(){ return permission; }

    public static Rank convertLevel(int level){
        return rankTypeMap.get(level);
    }

}
