package fr.eterya.api.packets;

import fr.eterya.api.params.Parametres;
import net.minecraft.server.v1_11_R1.IChatBaseComponent;
import net.minecraft.server.v1_11_R1.PacketPlayOutPlayerListHeaderFooter;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;

/**
 * Created by Arkas on 02/08/2017.
 * at 11:52
 */
public class PacketTabList {

    private String header;
    private String footer;

    public PacketTabList(Parametres parametres){
        this.header = ChatColor.translateAlternateColorCodes('&', (String) parametres.getConfig().get("tab.header"));
        this.footer = ChatColor.translateAlternateColorCodes('&', (String) parametres.getConfig().get("tab.footer"));
    }

    public PacketTabList setHeader(String header){
        this.header = header;
        return this;
    }

    public PacketTabList setFooter(String footer){
        this.footer = footer;
        return this;
    }

    public void sendPacket(Player player){
        if(this.header == null) this.header = ChatColor.GOLD + "Eterya";
        if(this.footer == null) this.footer = ChatColor.AQUA + "http://eterya.fr";

        final IChatBaseComponent header = IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + this.header + "\"}");
        final IChatBaseComponent footer = IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + this.footer + "\"}");
        final PacketPlayOutPlayerListHeaderFooter packetTab = new PacketPlayOutPlayerListHeaderFooter(header);
        Field f = null;
        try {
            f = packetTab.getClass().getDeclaredField("b");
            f.setAccessible(true);
            f.set(packetTab, footer);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packetTab);
    }

}
