package fr.eterya.api.items;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arkas on 11/03/2017.
 * at 12:19
 */
public class ItemBuilder {

    private ItemStack itemStack;
    private ItemMeta itemMeta;

    public ItemBuilder(){
        this(Material.AIR, 1, (short) 0);
    }

    public ItemBuilder(Material material){
        this(material, 1, (short) 0);
    }

    public ItemBuilder(Material material, short damage){
        this(material, 1, damage);
    }

    public ItemBuilder(Material material, int quantity, short damage){
        this.itemStack = new ItemStack(material, quantity, damage);
        this.itemMeta = this.itemStack.getItemMeta();
    }

    public ItemBuilder setDisplayName(String displayName){
        if(displayName == null) return this;
        this.itemMeta.setDisplayName(displayName);
        return this;
    }

    public ItemBuilder setOwner(String owner){
        if(owner == null) return this;
        SkullMeta skullMeta = (SkullMeta) this.itemMeta;
        skullMeta.setOwner(owner);
        this.itemStack.setItemMeta(skullMeta);
        return this;
    }

    public ItemBuilder setLore(List<String> lore){
        if(lore == null) return this;
        itemMeta.setLore(lore);
        return this;
    }

    public ItemBuilder setLores(String... lores) {
        List<String> desc = new ArrayList<String>();
        for (String lore : lores)
            desc.add(lore.replace('&','§'));
        itemMeta.setLore(desc);
        return this;
    }

    public ItemBuilder addFlag(ItemFlag flag){
        itemMeta.addItemFlags(flag);
        return this;
    }

    public ItemBuilder addFlags(ItemFlag... flags){
        itemMeta.addItemFlags(flags);
        return this;
    }

    public ItemBuilder addFlags(List<ItemFlag> flags){
        for(ItemFlag itemFlag : flags)
            addFlag(itemFlag);
        return this;
    }

    public ItemBuilder addEnchantment(Enchantment enchantment, int power, boolean ignoreLevel){
        itemMeta.addEnchant(enchantment, power, ignoreLevel);
        return this;
    }

    public ItemBuilder setUnbreakable(boolean unbreakable){
        this.itemMeta.spigot().setUnbreakable(unbreakable);
        return this;
    }

    public ItemStack getItem(){
        this.itemStack.setItemMeta(this.itemMeta);
        return this.itemStack;
    }

}
