package fr.eterya.api.items;

import fr.eterya.EteryaCore;
import fr.eterya.api.items.type.ItemRarity;
import fr.eterya.api.items.type.ItemType;
import fr.eterya.api.sql.DataBaseTable;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arkas on 10/04/2017.
 * at 12:37
 */
public class ItemDataBase extends DataBaseTable {

    public ItemDataBase() {
        super("ec_items");
        loadItems();
        loadItemsMc();
        EteryaCore.getApi().info("[EteryaCore] "+ReferencedItems.getIdMap().size()+" items loaded !");
    }

    private void loadItems(){
        //Ajout de l'air
        new ReferencedItems(1, new ItemBuilder(Material.AIR).getItem());

        Bukkit.getScheduler().runTaskAsynchronously(EteryaCore.getApi(), () ->{
            try {
                ResultSet resultSet = selectAllQuery("*");

                //Declaration
                int id = 2;
                int type = 50, rarity = 10;
                int min_attack, max_attack, speed_attack, level_required;
                int materialType = 0;
                short damaged = 0;
                int vitalite, regeneration, vitesse, resistance_cac, resistance_magie, dommages, magie, mana;

                while(resultSet.next()){
                    //On récupère l'id de l'Item
                    id = resultSet.getInt("ID_ITEM");
                    //On récupère le type
                    type = resultSet.getInt("TYPE");
                    //On récupère la rareté
                    rarity = resultSet.getInt("RARITY");
                    //On définit l'attaque et le level requis.
                    min_attack = resultSet.getInt("MIN_ATTACK");
                    max_attack = resultSet.getInt("MAX_ATTACK");
                    speed_attack = resultSet.getInt("SPEED_ATTACK");
                    level_required = resultSet.getInt("LEVEL_REQUIRED");
                    //On récupère le material
                    materialType = resultSet.getInt("MATERIAL_TYPE");
                    //On récupère le damage
                    damaged = resultSet.getShort("DAMAGED");

                    //On définit le type de l'arme
                    ItemType itemType = ItemType.valueOf(type);
                    //On définit la rareté de l'arme
                    ItemRarity itemRarity = ItemRarity.valueOf(rarity);
                    //On set le material
                    Material material = Material.getMaterial(materialType);

                    //On récupère les infos de l'ItemMeta
                    String displayName = resultSet.getString("DISPLAY_NAME");
                    if(displayName == null) displayName = material.name();
                    String[] description = resultSet.getString("LORES").split(";");

                    //On récupère les ItemFlags
                    List<ItemFlag> itemFlags = new ArrayList<>();
                    for(String flag : resultSet.getString("FLAGS").split(";")){
                        if(flag.isEmpty()) break;
                        itemFlags.add(ItemFlag.valueOf(flag));
                    }

                    //on récupère les enchantements
                    String list_enchants = resultSet.getString("ENCHANTMENTS");
                    String[] ench = new String[]{};
                    if (!list_enchants.isEmpty()) ench = list_enchants.split(";");

                    //On définit les statistique de l'item
                    vitalite = resultSet.getInt("VITALITE");
                    regeneration = resultSet.getInt("REGENERATION");
                    vitesse = resultSet.getInt("VITESSE");
                    resistance_cac = resultSet.getInt("RESISTANCE_CAC");
                    resistance_magie = resultSet.getInt("RESISTANCE_MAGIE");
                    dommages = resultSet.getInt("DOMMAGE");
                    magie = resultSet.getInt("MAGIE");
                    mana = resultSet.getInt("MANA");

                    //On définit si l'item est incassable ou non
                    boolean isUnbreakable = resultSet.getBoolean("isUnbreakable");

                    int attack[] = new int[]{min_attack, max_attack, speed_attack};
                    int caracs[] = new int[]{vitalite, regeneration, vitesse, resistance_cac, resistance_magie, dommages, magie, mana};

                    new ReferencedItems(id, itemType, itemRarity, attack, level_required, material, damaged, displayName, description, itemFlags, ench, isUnbreakable, caracs);
                }
                resultSet.getStatement().close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void loadItemsMc(){
        int ID = (ReferencedItems.getIdMap().size()+1);
        for(int mat = ID; mat < 501; mat++){
            Material material = Material.getMaterial(mat);
            if(material == null) material = Material.AIR;
            new ReferencedItems(ID, new ItemStack(material, 1));
            ID++;
        }
    }

    public void reload(EteryaCore eteryaCore){
        clearItems();
        eteryaCore.getEteryaManager().setItemDataBase(new ItemDataBase());
    }

    private void clearItems(){
        ReferencedItems.getIdMap().clear();
        ReferencedItems.getStringMap().clear();
    }

}
