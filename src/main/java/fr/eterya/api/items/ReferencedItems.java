package fr.eterya.api.items;

import fr.eterya.api.items.type.ItemRarity;
import fr.eterya.api.items.type.ItemType;
import fr.eterya.EteryaCore;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Arkas on 10/04/2017.
 * at 12:38
 */
public class ReferencedItems {

    private static Map<Integer, ReferencedItems> referencedIdItem = new HashMap<>();
    private static Map<String, ReferencedItems> referencedNameItem = new HashMap<>();

    private int id;
    private ItemStack itemStack;
    private ItemType itemType;
    private ItemRarity itemRarity;
    private int min_attack, max_attack, attack_speed;
    private int level_required;
    private int vitalite;
    private int regeneration;
    private int vitesse;
    private int resistance_cac;
    private int resistance_magie;
    private int dommages;
    private int magie;
    private int mana;

    public ReferencedItems(int id, ItemStack itemStack){
        this.id = id;

        this.itemType = ItemType.UNKNOWN;
        this.itemRarity = ItemRarity.COMMON;

        this.min_attack = 0;
        this.max_attack = 0;
        this.attack_speed = 0;

        this.level_required = 0;

        this.vitalite = 0;
        this.regeneration = 0;
        this.vitesse = 0;
        this.resistance_cac = 0;
        this.resistance_magie = 0;
        this.dommages = 0;
        this.magie = 0;
        this.mana = 0;

        //On créer l'item
        this.itemStack = itemStack;

        //On stock tout
        referencedIdItem.put(id, this);
        referencedNameItem.put("null-"+id, this);
    }

    public ReferencedItems(int id, ItemType type, ItemRarity rarity, int[] attack, int level, Material material, short damaged, String displayName,
                           String[] lores, List<ItemFlag> itemFlags, String[] enchantments, boolean isUnbreakable, int... caracs){
        this.id = id;

        this.itemType = type;
        this.itemRarity = rarity;

        this.min_attack = attack[0];
        this.max_attack = attack[1];
        this.attack_speed = attack[2];

        this.level_required = level;

        if(caracs.length > 7) {
            this.vitalite = caracs[0];
            this.regeneration = caracs[1];
            this.vitesse = caracs[2];
            this.resistance_cac = caracs[3];
            this.resistance_magie = caracs[4];
            this.dommages = caracs[5];
            this.magie = caracs[6];
            this.mana = caracs[7];
        } else {
            this.vitalite = caracs[0];
            this.regeneration = caracs[0];
            this.vitesse = caracs[0];
            this.resistance_cac = caracs[0];
            this.resistance_magie = caracs[0];
            this.dommages = caracs[0];
            this.magie = caracs[0];
            this.mana = caracs[0];
        }

        ItemBuilder itemBuilder = new ItemBuilder(material, damaged);
        //On set le displayName
        if(!displayName.isEmpty()) itemBuilder.setDisplayName(displayName);

        //On ajoute le unbreakable
        itemBuilder.setUnbreakable(isUnbreakable);

        List<String> item_lore = new ArrayList<>();
        //On ajoute le type et la rareté de l'item
        item_lore.add("§bType: "+type.getName() + "    §9Rarity: "+rarity.getName());
        //On ajoute l'attaque et la vitesse d'attaque
        if(attack[0] != 0 && attack[1] != 0){ item_lore.add(""); item_lore.add("§eAttack: §f"+attack[0]+"§6-§f"+attack[1]+"    §bAttack Speed: "+attack[2]+"%"); }
        //On ajoute le niveau requis
        if(level != 0) item_lore.add("§cLevel required: "+level);

        for(int i = 0; i < caracs.length; i++){ if(caracs[i] != 0){ item_lore.add(""); break; } }
        if(vitalite != 0) item_lore.add("§4❤ Health: §f"+vitalite);
        if(regeneration != 0) item_lore.add("§c✦ Regeneration: §f"+regeneration);
        if(vitesse != 0) item_lore.add("§b» Move Speed: §f"+vitesse+"%");
        if(resistance_cac != 0) item_lore.add("§8❚ Resistance Melee: §f"+ -resistance_cac+"%");
        if(resistance_magie != 0) item_lore.add("§5✮ Resistance Magic: §f"+ -resistance_magie+"%");
        if(dommages != 0) item_lore.add("§2✸ Damage: §f"+dommages);
        if(magie != 0) item_lore.add("§d❈ Magic: §f"+magie);
        if(mana != 0) item_lore.add("§9❋ Mana: §f"+mana);

        if(lores.length != 0 && lores[0].length() >= 4){
            item_lore.add("");
            for(String line : lores) item_lore.add("§7"+line);
        }
        //On ajoute la description
        itemBuilder.setLore(item_lore);

        //On ajoute les items flags
       // itemBuilder.addFlags(itemFlags);
        //Par défaut on ajoute le flag hide_attributes
        itemBuilder.addFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS);

        //On ajoute les enchantements
        if(enchantments.length != 0){
            if(enchantments.equals("") || enchantments.equals(" ")) return;
            try {
                for (int i = 0; i < enchantments.length; i++) {
                    String[] value = enchantments[i].split(",");
                    itemBuilder.addEnchantment(Enchantment.getById(Integer.parseInt(value[0])), Integer.parseInt(value[1]), Boolean.parseBoolean(value[2]));
                }
            } catch (NumberFormatException e){
                EteryaCore.getApi().severe("Error lors de l'enregistrement d'un item. ID : "+id+" | Error : "+e.getMessage());
            }
        }

        //On créer l'item
        this.itemStack = itemBuilder.getItem();

        //On stock tout
        referencedIdItem.put(id, this);
        referencedNameItem.put(displayName, this);
    }

    public static Map<Integer, ReferencedItems> getIdMap(){
        return referencedIdItem;
    }

    public static Map<String, ReferencedItems> getStringMap() {
        return referencedNameItem;
    }

    public boolean isItemExist(){
        return referencedIdItem.containsKey(id);
    }

    public int getId(){
        return id;
    }

    public ItemStack getItem(){
        return isItemExist() ? itemStack : new ItemBuilder().getItem();
    }

    public int getMinAttack() {
        return min_attack;
    }
    public int getMaxAttack() {
        return max_attack;
    }
    public int getAttackSpeed() {
        return attack_speed;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public ItemRarity getItemRarity() {
        return itemRarity;
    }

    public int getVitalite(){
        return vitalite;
    }

    public int getRegeneration() {
        return regeneration;
    }

    public int getVitesse() {
        return vitesse;
    }

    public int getResistanceCac() {
        return resistance_cac;
    }

    public int getResistanceMagie() {
        return resistance_magie;
    }

    public int getDommages() {
        return dommages;
    }

    public int getMagie() {
        return magie;
    }

    public int getMana() {
        return mana;
    }

    public static ReferencedItems valueOf(int id){
        return referencedIdItem.get(id);
    }
    public static ReferencedItems valueOf(String displayName){
        return referencedNameItem.get(displayName);
    }

    public static String convertInventoryToString(Inventory inventory){

        StringBuilder builder = new StringBuilder();

        //Boucle pour récupèrer les items de l'inventaire
        for(ItemStack itemStack : inventory.getContents()){

            //On effectue une boucle pour récupérer les ids
            for(int id = 1; id <= getIdMap().size(); id++){

                //On vérifie que l'item n'est pas null
                if(itemStack == null || itemStack.getType() == Material.AIR){
                    builder.append("1:0,");
                    break;
                }

                ReferencedItems referencedItems = valueOf(id);

                //On vérifie si l'item est similaire à celui référencer
                if(referencedItems.getItem().isSimilar(itemStack)){
                    builder.append(id + ":" + itemStack.getAmount() + ",");
                    break;
                }

                //Si l'item n'est pas enregistrer on mais une case vide
                if(id == getIdMap().size()){
                    builder.append("1:0,");
                }
            }
        }

        return builder.toString();
    }

}
