package fr.eterya.api.items.type;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Arkas on 01/05/2017.
 * at 12:23
 */
public enum  ItemRarity {

    COMMON(0, "§7Common"),
    NORMAL(1, "§fNormal"),
    RARE(2, "§3Rare"),
    EPIC(3, "§5Epic"),
    LEGENDARY(4, "§6Legendary"),
    UNKNOWN(10, "§eUnknown"),
    UNKNOWN2(11, "");

    private int id;
    private String name;

    private static Map<Integer, ItemRarity> itemRarityMap = new HashMap<>();

    ItemRarity(int id, String name){
        this.id = id;
        this.name = name;
    }

    static {
        for(ItemRarity itemRarity : ItemRarity.values()){
            itemRarityMap.put(itemRarity.id, itemRarity);
        }
    }

    private static Map<Integer, ItemRarity> getRarityMap() {
        return itemRarityMap;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static ItemRarity valueOf(int id){
        return getRarityMap().containsKey(id) ? getRarityMap().get(id) : COMMON;
    }
}
