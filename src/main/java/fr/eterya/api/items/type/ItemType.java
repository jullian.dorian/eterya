package fr.eterya.api.items.type;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Arkas on 01/05/2017.
 * at 11:46
 */
public enum  ItemType {

    //Corps à corps
    DAGGER(0, "§fDagger"),
    KATANA(1, "§fKatana"),
    SPEAR(2, "§7Spear"),
    SCYTHE(3, "§5Scythe"),
    SWORD(4, "§8Sword"),
    AXE(5, "§7Axe"),
    GREAT_SWORD(6, "§8Great Sword"),
    HAMMER(7, "§fHammer"),
    HALBERDS(8, "§7Halberds"),
    GREAT_AXE(9, "§7Great Axe"),
    ULTRA_GREATSWORD(10, "§8Ultra Great Sword"),
    GREAT_HAMMER(11, "§8Great Hammer"),

    //Arcs
    LIGHT_BOW(12, "§aLight Bow"),
    BOW(13, "§1Bow"),
    GREAT_BOW(14, "§6Great Bow"),

    //Boucliers
    SMALL_SHIELD(15, "§7Small Shield"),
    SHIELD(16, "§8Shield"),
    GREAT_SHIELD(17, "§7Great Shield"),

    //Bâtons
    TALISMAN(18, "§aTalisman"),
    CHIME(19, "§bChime"),
    STAFF(20, "§9Staff"),
    GREAT_STAFF(21, "§6Great Staff"),

    HELMET(30, "§7Helmet"),
    CHESTPLATE(31, "§7Chestplate"),
    LEGGINGS(32, "§7Leggings"),
    BOOTS(33, "§7Boots"),

    UNKNOWN(50, "§8Unkown"),
    UNKNOWN2(51, "");

    private int id;
    private String name;

    private static Map<Integer, ItemType> typeHashMap = new HashMap<>();

    ItemType(int id, String name){
        this.id = id;
        this.name = name;
    }

    static {
        for(ItemType type : ItemType.values()){
            typeHashMap.put(type.id, type);
        }
    }

    private static Map<Integer, ItemType> getTypeMap() {
        return typeHashMap;
    }

    /**
     * Get the ID of the ItemType
     * @return id Is the id of type
     */
    public int getId() {
        return id;
    }

    /**
     * Get the name of ItemType
     * @return name
     */
    public String getName() {
        return name;
    }

    public static ItemType valueOf(int id){
        return getTypeMap().containsKey(id) ? getTypeMap().get(id) : UNKNOWN;
    }
}
