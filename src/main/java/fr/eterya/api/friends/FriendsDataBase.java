package fr.eterya.api.friends;

import fr.eterya.api.sql.DataBaseTable;

/**
 * Created by Arkas on 15/07/2017.
 * at 16:58
 */
public class FriendsDataBase extends DataBaseTable {

    public FriendsDataBase() {
        super("ec_friends");
    }
}
