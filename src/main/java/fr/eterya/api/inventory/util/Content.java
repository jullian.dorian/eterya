package fr.eterya.api.inventory.util;

import org.bukkit.inventory.ItemStack;

/**
 * Created by Arkas on 24/05/2017.
 * at 11:29
 */
public class Content {

    int slot;
    ItemStack itemStack;

    public Content(int slot, ItemStack itemStack){
        this.slot = slot;
        this.itemStack = itemStack;
    }

    public int getSlot(){
        return slot;
    }

    public ItemStack getItem(){
        return itemStack;
    }

}
