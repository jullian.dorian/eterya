package fr.eterya.api.inventory.util;

import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Arkas on 27/03/2017.
 * at 20:42
 */
public abstract class AbstractInventory{

    private Inventory inventory;

    public AbstractInventory(int size, String title){
        this.inventory = Bukkit.createInventory(null, size, title);
    }
    public AbstractInventory(InventoryType inventoryType, String title) {
        this.inventory = Bukkit.createInventory(null, inventoryType, title);
    }

    protected abstract Content[] content();

    protected void setItem(int slot, ItemStack itemStacks){
        this.inventory.setItem(slot, itemStacks);
    }

    protected void setContent(Content[] contents){
        if(contents == null) return;
        for(Content content : contents){
            if(content == null || (content.getSlot() < 0 || content.getItem() == null)) return;
            if(content != null || (content.getSlot() > 0 && content.getItem() != null))
                setItem(content.getSlot(), content.getItem());
        }
    }

    public String getTitle() {
        return this.getInventory().getTitle();
    }

    public int getSize() {
        return inventory.getSize();
    }

    public Inventory getInventory() {
        return inventory;
    }

    public abstract void inventoryAction(InventoryClickEvent event);

}
