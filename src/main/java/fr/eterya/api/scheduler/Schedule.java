package fr.eterya.api.scheduler;

import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Arkas on 20/03/2017.
 * at 13:23
 */
public class Schedule extends BukkitRunnable {

    private Plugin plugin;

    public Schedule(Plugin plugin){ this.plugin = plugin;}

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {

    }
}
