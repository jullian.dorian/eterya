package fr.eterya.api.npc;


import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;

/**
 * Created by Arkas on 06/05/2017.
 * at 16:03
 */
public class VillagerBuilder {

    private int ID;
    private String displayName;
    private boolean asAI;
    private Villager villager;

    public VillagerBuilder(Location spawn, EntityType entityType){
        if(entityType != EntityType.VILLAGER) entityType = EntityType.VILLAGER;
        this.villager = (Villager) spawn.getWorld().spawnEntity(spawn, entityType);
        //this.ID = (EteryaCore.getApi().npcBuilderMap.size()+1);
    }

    public VillagerBuilder setProfession(Villager.Profession profession){
        if(profession == null) profession = Villager.Profession.FARMER;
        this.villager.setProfession(profession);
        return this;
    }

    public VillagerBuilder setBaby(boolean value){
        if(value) villager.setBaby();
        return this;
    }

    public VillagerBuilder setCustomName(String displayName){
        this.displayName = displayName;
        this.villager.setCustomName(displayName);
        return this;
    }

    public VillagerBuilder setCustomNameVisible(boolean value){
        this.villager.setCustomNameVisible(value);
        return this;
    }

    public VillagerBuilder setNoAI(boolean value){
        this.asAI = value;
        this.villager.setAI(value);
        return this;
    }

    public VillagerBuilder setInvulnerable(boolean value){
        this.villager.setInvulnerable(value);
        return this;
    }

    public int getID(){
        return ID;
    }

    public Villager getVillager(){
        return villager;
    }

    public boolean hasAi(){
        return asAI;
    }

    /*public void create(){
        EteryaCore.getApi().npcBuilderMap.put(this.ID, this);
    }*/

}
