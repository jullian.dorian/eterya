package fr.eterya.api.npc;

import net.minecraft.server.v1_11_R1.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

/**
 * Created by Arkas on 06/05/2017.
 * at 16:03
 */
public class NPCBuilder {

    private String name;
    private Location location;

    private EntityPlayer entity;
    private boolean hasAi;
    private boolean isInvulnerable;


    public NPCBuilder(String name, Location location, EntityPlayer entity) {
        this.name = name;
        this.location = location;
        this.entity = entity;
    }

    public NPCBuilder setAI(boolean hasAi) {
        this.hasAi = hasAi;
        return this;
    }

    public NPCBuilder setInvulnerable(boolean invulnerable) {
        this.isInvulnerable = invulnerable;
        return this;
    }

    public void createPlayer(Player player){
        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
        connection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, entity));
        connection.sendPacket(new PacketPlayOutNamedEntitySpawn(entity));
        connection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, entity));
    }

    public String getName() {
        return name;
    }

    public Location getLocation() {
        return location;
    }

    public EntityPlayer getEntity() {
        return entity;
    }
}
