package fr.eterya.api.npc;

import net.minecraft.server.v1_11_R1.Entity;
import net.minecraft.server.v1_11_R1.EntityPlayer;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Arkas on 03/08/2017.
 * at 14:24
 */
public class NPCManager {

    Set<NPCBuilder> npcBuilders = new HashSet<>();
    private Plugin plugin;

    public NPCManager(Plugin plugin){
        this.plugin = plugin;
    }

    public void createNpc(Player player, Location location, Entity entity){
        if(entity instanceof EntityPlayer) {
            Player entityPlayer = ((EntityPlayer) entity).getBukkitEntity().getPlayer();
            entityPlayer.setPlayerListName("");

            NPCBuilder npcBuilder = new NPCBuilder(entity.getName(), location, (EntityPlayer) entityPlayer);
            npcBuilder.createPlayer(player);

            npcBuilders.add(npcBuilder);
        }
    }

}
